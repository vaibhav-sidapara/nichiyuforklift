<?php

class Get_news_model extends CRUD {

    protected $table = 'news';
    protected $_primary_key = 'id';

    public function __construct()
    {
        parent::__construct();
    }

    /*
     * SELECT
    MONTHNAME(t.summaryDateTime) as month, YEAR(t.summaryDateTime) as year
FROM
    trading_summary t
GROUP BY YEAR(t.summaryDateTime), MONTH(t.summaryDateTime) DESC";
     */

    public function get_news($year = NULL, $month = NULL, $slug = NULL)
    {
        $this->db->select('name as name');
        $this->db->select('MONTHNAME(date) as month');
        $this->db->select('Year(date) as year');
        $this->db->select('description as description');
        $this->db->select('image as image');
        $this->db->select('slug as slug');
        $this->db->from('news');
        $this->db->order_by("date DESC");
        if($year != NULL) { $this->db->where('YEAR(date) =', $year); }
        if($month != NULL) { $this->db->where('MONTH(date) =', $month); }
        if($slug != NULL) {
            $this->db->select('details as details');
            $this->db->where('slug =', $slug);
            return $this->db->get()->row();
        }

        return $this->db->get()->result_array();
    }

    public function get_news_archive_list()
    {
        $this->db->select('YEAR(date) as year');
        $this->db->from('news');
        $this->db->group_by("YEAR(date) DESC");

        $news_archive = $this->db->get()->result_array();
        $details = [];
        foreach ($news_archive as $year) {
            if( isset($year['year'] ) && !empty($year['year']) ) {
                $details[$year['year']] = $this->Get_news_model->get_news_month_from_year($year['year']);
            }
        }

        return $details;
    }

    public function get_news_month_from_year($year = NULL)
    {
        $this->db->select('MONTHNAME(date) as month');
        $this->db->select('MONTH(date) as month_num');
        $this->db->from('news');
        $this->db->where('YEAR(date) =', "$year");
        $this->db->group_by("MONTH(date) DESC");
        return $this->db->get()->result_array();
    }

}