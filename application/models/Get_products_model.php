<?php

class Get_products_model extends CRUD {

    protected $table = '';
    protected $_primary_key = 'id';

    public function __construct()
    {
        parent::__construct();
    }

    public function get_product_details($slug)
    {
        $data = (object) '';
        $this->db->select('*');
        $this->db->from('products');
        $this->db->where('slug =', $slug);
        $data->product = $product = $this->db->get()->row();

        if(empty($data->product)) { return FALSE; }

        if($product->sub_category_id !== 0 ) {
            $data->sub_category = $this->get_product_sub_category($product->sub_category_id);
        }

        $data->category = $this->get_product_category($product->category_id);

        $data->models = $this->get_product_model($product->id);

        $data->images = $this->get_product_images($product->id);

        return $data;
    }

    public function get_product_model($id)
    {
        $this->db->select('*');
        $this->db->from('product_models');
        $this->db->where('product_id =', $id);

        return $this->db->get()->result_array();
    }

    public function get_product_images($id)
    {
        $this->db->select('*');
        $this->db->from('product_images');
        $this->db->where('product_id =', $id);

        return $this->db->get()->result_array();
    }

    public function get_product_category($data, $row = TRUE)
    {
        $this->db->select('*');
        $this->db->from('product_category');
        ( is_numeric($data) )? $this->db->where('id =', $data) : $this->db->where('slug =', $data);

        ( $row )? $result = $this->db->get()->row() : $result = $this->db->get()->result_array();

        return $result;
    }

    public function get_product_sub_category($data, $row = TRUE)
    {
        $this->db->select('*');
        $this->db->from('product_sub_category');
        ( is_numeric($data) )? $this->db->where('id =', $data) : $this->db->where('slug =', $data);

        ( $row )? $result = $this->db->get()->row() : $result = $this->db->get()->result_array();

        return $result;
    }

    public function get_product_sub_category_by_category_id($id)
    {
        $this->db->select('*');
        $this->db->from('product_sub_category');
        $this->db->where('category_id =', $id);

        return $this->db->get()->result_array();
    }

    public function get_product_products_by_sub_category_id($id)
    {
        $this->db->select('*');
        $this->db->from('products');
        $this->db->where('sub_category_id =', $id);

        return $this->db->get()->result_array();
    }

    public function get_product_products_by_category_id($id)
    {
        $this->db->select('*');
        $this->db->from('products');
        $this->db->where('category_id =', $id);

        return $this->db->get()->result_array();
    }

}