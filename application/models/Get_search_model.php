<?php

class Get_search_model extends CRUD {

    protected $table = '';
    protected $_primary_key = 'id';

    public function __construct()
    {
        parent::__construct();
    }

    public function general_search($search)
    {
        $this->db->select('name as name');
        $this->db->select('MONTHNAME(date) as month');
        $this->db->select('Year(date) as year');
        $this->db->select('description as description');
        $this->db->select('image as image');
        $this->db->select('slug as slug');
        $this->db->from('news');
        $this->db->order_by("date DESC");
        $this->db->or_like('description', $search);
        $this->db->or_like('details', $search);
        $this->db->or_like('name', $search);

        return $this->db->get()->result_array();
    }

    public function category_search($search)
    {
        $this->db->select('*');
        $this->db->from('product_category');
        $this->db->or_like('slug', $search);
        $this->db->or_like('name', $search);

        return $this->db->get()->result_array();
    }

    public function sub_category_search($search)
    {
        $this->db->select('*');
        $this->db->from('product_sub_category');
        $this->db->or_like('slug', $search);
        $this->db->or_like('name', $search);

        return $this->db->get()->result_array();
    }

    public function products_search($search)
    {
        $this->db->select('*');
        $this->db->from('products');
        $this->db->or_like('slug', $search);
        $this->db->or_like('name', $search);
        $this->db->or_like('capacity', $search);
        $this->db->or_like('description', $search);
        $this->db->or_like('features', $search);

        return $this->db->get()->result_array();
    }

}