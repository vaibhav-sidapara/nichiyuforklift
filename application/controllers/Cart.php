<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    private function _throwJSONOutput($resultValue, $outputData = NULL, $outputName = 'data')
    {
        $this->output->set_content_type('application/json');
        $messageArray = $outputData;
        die( json_encode(array("result" => $resultValue, $outputName => $messageArray)) );
    }

    public function index()
    {
        $this->load->model('Get_products_model');
        $cart = $this->session->userdata('cart');
        $products_models = [];
        if( isset($cart)) {
            foreach ($cart as $product) {
                $products_models[$product['slug']] = $this->Get_products_model->get_product_model($product['id']);
            }
        }

        $this->load->view('inc/header_view', ['title' => 'Enquiry Cart | Mitsubishi Nichiyu Electric Forklift']);
        $this->load->view('enquiry_cart_view', ['products_models' => $products_models, 'cart' => $cart]);
        $this->load->view('inc/footer_view');
    }

    public function add_to_cart()
    {
        $cart = $this->session->userdata('cart');
        if( !isset( $cart ) ) {
            $this->session->set_userdata(['cart' => [$this->input->post('slug') => $this->input->post()] ]);
        } else {
            $cart[$this->input->post('slug')] = $this->input->post();
            $this->session->set_userdata(['cart' => $cart]);
        }

        $this->_throwJSONOutput(1, count($cart), 'data');
    }

    public function remove($slug)
    {
        $cart = $this->session->userdata('cart');
        unset($cart[$slug]);
        $this->session->set_userdata(['cart' => $cart]);

        redirect(base_url('enquiry-cart'));
    }

    public function edit($slug)
    {
        $cart = $this->session->userdata('cart');
        $cart[$slug]['model'] = $this->input->post('model');
        $this->session->set_userdata(['cart' => $cart]);

        $this->_throwJSONOutput(1, 'success', 'data');
    }

    public function submit()
    {
        $this->form_validation->set_rules('name','Name','required');
        $this->form_validation->set_rules('email','Email','required|valid_email');
        $this->form_validation->set_rules('tel','Phone','required');
        $this->form_validation->set_rules('country','Country','required');
        $this->form_validation->set_rules('comments','Comments','required');

        if($this->form_validation->run() == FALSE) {
            $message = strip_tags(validation_errors());
            $this->_throwJSONOutput(0, $message);
        }

        $data = $this->input->post();
        $data['cart'] = serialize($this->session->userdata('cart'));

        $this->load->model('Set_enquiry_model');
        if( $this->Set_enquiry_model->insert($data) ) {
            $this->session->unset_userdata('cart');
            $this->_throwJSONOutput(1, 'success');
        }

    }

}
