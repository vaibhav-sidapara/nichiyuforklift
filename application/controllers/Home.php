<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends My_Controller
{

    private function _throwJSONOutput($resultValue, $outputData = NULL, $outputName = 'data')
    {
        $this->output->set_content_type('application/json');
        $messageArray = $outputData;
        die( json_encode(array("result" => $resultValue, $outputName => $messageArray)) );
    }

    public function welcome()
    {
        $this->load->view('inc/header_view', ['title' => 'NICHIYU  ELECTRIC  FORKLIFT']);
        $this->load->view('home_view');
        $this->load->view('inc/footer_view');
    }

    public function services_support()
    {
        $this->load->view('inc/header_view', ['title' => 'Services & Support | Mitsubishi Nichiyu Electric Forklift', 'banner_image' => 'banner-services.jpg']);
        $this->load->view('services_support_view');
        $this->load->view('inc/footer_view');
    }

    public function contact_us()
    {
        if( empty( $this->input->post() ) ) {
            $this->load->view('inc/header_view', ['title' => 'Contact Us | Mitsubishi Nichiyu Electric Forklift', 'banner_image' => 'banner-contact.jpg']);
            $this->load->view('contact_us_view');
            $this->load->view('inc/footer_view');
        }

        if( $this->input->post() ) {
            $this->form_validation->set_rules('name','Name','required');
            $this->form_validation->set_rules('email','Email','required|valid_email');
            $this->form_validation->set_rules('tel','Phone','required');
            $this->form_validation->set_rules('country','Country','required');
            $this->form_validation->set_rules('comments','Comments','required');

            if($this->form_validation->run() == FALSE) {
                $message = strip_tags(validation_errors());
                $this->_throwJSONOutput(0, $message);
            }

            $this->load->model('Set_contact_us_model');
            if( $this->Set_contact_us_model->insert($this->input->post()) ) {
                $this->_throwJSONOutput(1, 'success');
            }
        }
    }

    public function dealer_locator()
    {
        $this->load->view('inc/header_view', ['title' => 'Dealer Locator | Mitsubishi Nichiyu Electric Forklift', 'banner_image' => 'banner-location.jpg']);
        $this->load->view('dealer_locator_view');
        $this->load->view('inc/footer_view');
    }

    public function thank_you()
    {
        $this->load->view('inc/header_view', ['title' => 'Thank you | Mitsubishi Nichiyu Electric Forklift', 'banner_image' => 'banner-contact.jpg']);
        $this->load->view('thank_you_view');
        $this->load->view('inc/footer_view');
    }

    public function search()
    {
        $this->load->model('Get_search_model');
        if($this->input->post('search-option') == 'general') {
            $news_archive = $this->Get_search_model->general_search($this->input->post('keyword'));

            $this->load->view('inc/header_view', ['title' => 'Search | Mitsubishi Nichiyu Electric Forklift', 'banner_image' => 'banner-contact.jpg']);
            $this->load->view('search_view',
                [
                    'search_keyword' => $this->input->post('keyword'),
                    'news_archive' => ( !empty($news_archive) )?$news_archive:NULL,
                ]
            );
            $this->load->view('inc/footer_view');
        }

        if($this->input->post('search-option') == 'category') {
            $categories = $this->Get_search_model->category_search($this->input->post('keyword'));

            $this->load->view('inc/header_view', ['title' => 'Search | Mitsubishi Nichiyu Electric Forklift', 'banner_image' => 'banner-news.jpg']);
            $this->load->view('search_view',
                [
                    'search_keyword' => $this->input->post('keyword'),
                    'categories' => ( !empty($categories) )?$categories:NULL,
                ]
            );
            $this->load->view('inc/footer_view');
        }

        if($this->input->post('search-option') == 'sub-category') {
            $sub_categories = $this->Get_search_model->sub_category_search($this->input->post('keyword'));

            $this->load->view('inc/header_view', ['title' => 'Search | Mitsubishi Nichiyu Electric Forklift', 'banner_image' => 'banner-news.jpg']);
            $this->load->view('search_view',
                [
                    'search_keyword' => $this->input->post('keyword'),
                    'sub_categories' => ( !empty($sub_categories) )?$sub_categories:NULL,
                ]
            );
            $this->load->view('inc/footer_view');
        }

        if($this->input->post('search-option') == 'product') {
            $products = $this->Get_search_model->products_search($this->input->post('keyword'));

            $this->load->view('inc/header_view', ['title' => 'Search | Mitsubishi Nichiyu Electric Forklift', 'banner_image' => 'banner-news.jpg']);
            $this->load->view('search_view',
                [
                    'search_keyword' => $this->input->post('keyword'),
                    'products' => ( !empty($products) )?$products:NULL,
                ]
            );
            $this->load->view('inc/footer_view');
        }

    }

}
