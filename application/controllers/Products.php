<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends My_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->load->view('inc/header_view', ['title' => 'Products | Mitsubishi Nichiyu Electric Forklift', 'banner_image' => 'banner-product.jpg']);
        $this->load->view('products_category_view', ['active_slug' => NULL]);
        $this->load->view('inc/footer_view');
    }

    public function category($slug)
    {
        $this->load->model('Get_products_model');
        $id = $this->Get_products_model->get_product_category($slug)->id;
        if(empty($id)) { redirect(base_url()); }
        $sub_category = $this->Get_products_model->get_product_sub_category_by_category_id($id);

        $this->load->view('inc/header_view', ['title' => 'Products | Mitsubishi Nichiyu Electric Forklift', 'banner_image' => 'banner-product.jpg']);
        $this->load->view('products_sub_category_view', ['sub_category' => $sub_category, 'active_slug' => $slug]);
        $this->load->view('inc/footer_view');
    }

    public function sub_category($slug)
    {
        $this->load->model('Get_products_model');
        $id = $this->Get_products_model->get_product_sub_category($slug)->id;
        if(empty($id)) { redirect(base_url()); }
        $products = $this->Get_products_model->get_product_products_by_sub_category_id($id);

        $this->load->view('inc/header_view', ['title' => 'Products | Mitsubishi Nichiyu Electric Forklift', 'banner_image' => 'banner-product.jpg']);
        $this->load->view('products_view', ['products' => $products, 'active_slug' => $slug]);
        $this->load->view('inc/footer_view');
    }

    public function detail($slug)
    {
        $this->load->model('Get_products_model');
        $data = $this->Get_products_model->get_product_details($slug);
        if(empty($data)) { redirect(base_url()); }

        $this->load->view('inc/header_view', ['title' => 'Products | Mitsubishi Nichiyu Electric Forklift']);
        $this->load->view('product_detail_view', ['data' => $data, 'active_slug' => $slug]);
        $this->load->view('inc/footer_view');
    }


}
