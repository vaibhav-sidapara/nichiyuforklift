<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends My_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Get_news_model');
    }

    public function index($year = NULL, $month = NULL)
    {
        $news_archive_list = $this->Get_news_model->get_news_archive_list();
        $news_archive = $this->Get_news_model->get_news($year, $month, NULL);

        $this->load->view('inc/header_view', ['title' => 'Latest News | Mitsubishi Nichiyu Electric Forklift', 'banner_image' => 'banner-news.jpg']);
        $this->load->view('latest_news_view',
            [
                'news_archive_list' => $news_archive_list,
                'news_archive' => $news_archive,
                'month_active' => ($month == NULL)? NULL : date('F', mktime(0, 0, 0, $month, 10)),
                'year_active' => ($year == NULL)? NULL : $year,
            ]
        );
        $this->load->view('inc/footer_view');
    }

    public function details($slug)
    {
        $news_archive = $this->Get_news_model->get_news(NULL, NULL, $slug);
        $news_archive_list = $this->Get_news_model->get_news_archive_list();

        $this->load->view('inc/header_view', ['title' => 'Latest News | Mitsubishi Nichiyu Electric Forklift', 'banner_image' => 'banner-news.jpg']);
        $this->load->view('news_detail_view',
            [
                'news_archive_list' => $news_archive_list,
                'news_archive' => $news_archive,
                'month_active' => $news_archive->month,
                'year_active' => $news_archive->year,
            ]
        );
        $this->load->view('inc/footer_view');
    }

}
