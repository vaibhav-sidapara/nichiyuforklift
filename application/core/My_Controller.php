<?php

class My_Controller extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $categories_structure = $this->session->userdata('categories_structure');
        if( !isset( $categories_structure ) ) {
            $this->session->set_userdata(['categories_structure' => $this->_get_categories()]);
        }

    }

    private function _get_categories()
    {
        $query = $this->db->get('product_category');
        $return = array();

        foreach ($query->result() as $category)
        {
            $return[$category->id] = $category;
            $return[$category->id]->sub_category = $this->_get_sub_categories($category->id); // Get the categories sub categories
        }

        return $return;
    }


    private function _get_sub_categories($category_id)
    {
        $this->db->where('category_id', $category_id);
        $query = $this->db->get('product_sub_category');
        return $query->result();
    }

}