<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<link href="<?=base_url('public/')?>css/tad.css" rel="stylesheet" type="text/css" />
<div class="page-with-banner">
    <div class="clear_30"></div>
    <div class="container">
        <div class="bred-camb">
            <a href="<?=base_url('')?>">Home</a>
            <span class="seprster fa fa-angle-right"></span>
            <a href="<?=base_url('products')?>">Our Products</a>
            <span class="seprster fa fa-angle-right"></span>
            <a href="<?=base_url('products/category/').$data->category->slug?>"><?=$data->category->name?></a>
            <span class="seprster fa fa-angle-right"></span>
            <?php if( isset($data->sub_category) ) { ?>
                <a href="<?=base_url('products/sub-category/').$data->sub_category->slug?>"><?=$data->sub_category->name?></a>
                <span class="seprster fa fa-angle-right"></span>
            <?php } ?>
            <a href="<?=base_url('products/details/').$data->product->slug?>"><?=$data->product->name?></a>
            <span class="seprster fa fa-angle-right"></span>Product Details</div>
        <div class="detail_wrap clearfix">
            <div class="detail_left">
                <div class="view_image">
                    <img src="<?=base_url('public/').$data->product->image?>" id="View_image">
                </div>
                <div class="Thumb_wrap">
                    <div class="d-carousel-thumb">
                        <ul class="carousel">
                            <li>
                                <div class="thumb_box"><a href="<?=base_url('public/').$data->product->image?>" class="active"><img src="<?=base_url('public/').$data->product->image?>" alt=""></a></div>
                            </li>
                            <?php foreach ($data->images as $row) { ?>
                                <li>
                                    <div class="thumb_box"><a href="<?=base_url('public/').$row['images']?>" class="active"><img src="<?=base_url('public/').$row['images']?>" alt=""></a></div>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="detail_right">
                <h2 class="page-title">Junior-T</h2>
                <div class="tabs clearfix">
                    <ul class="tabNavigation">
                        <li><a href="#one">DESCRIPTION</a></li>
                        <li><a href="#two">KEY FEATURES</a></li>
                    </ul>
                    <div id="one" class="one">
                        <div class="Description">
                            <?=$data->product->description?>
                        </div>
                    </div>

                    <div id="two" class="one">
                        <div class="specifications">
                            <?=$data->product->features?>
                        </div>
                    </div>
                </div>
                <form class="product_details_form" action="<?=base_url('cart/add_to_cart')?>" method="POST">
                    <div class="product-info">
                        <ul>
                            <li><span>Capacity:</span><?=$data->product->capacity?></li>
                            <li>
                                <span>Model:</span>
                                <div class="select-model ">
                                    <select title="model" name="model">
                                        <?php foreach ($data->models as $row) { ?>
                                        <option value="<?=$row['name']?>" ><?=$row['name']?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="btn-wrap  clearfix">

                            <a  href="<?=base_url('products')?>" class="continue button left" style="text-align: center">CONTINUE</a>

                            <input type="hidden" name="id" value="<?=$data->product->id?>">
                            <input type="hidden" name="slug" value="<?=$data->product->slug?>">
                            <input type="hidden" name="name" value="<?=$data->product->name?>">
                            <input type="hidden" name="description" value="<?=$data->product->description?>">
                            <input type="hidden" name="image" value="<?=$data->product->image?>">
                            <input type="hidden" name="capacity" value="<?=$data->product->capacity?>">
                            <span class="message-response enquiry_cart"></span>
                            <button type="submit" class="addto_enquire right">ADD TO ENQUIRY</button>
                    </div>
                </form>
            </div>

        </div>
        <?php if( !empty($data->product->video) ) { ?>
        <div class="Take-Look">
            <h3>Take a Look</h3>
            <div class="video">
                <iframe src="<?=$data->product->video?>" width="500" height="400" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></p>
            </div>
        </div>
        <?php } ?>
    </div>
</div>

<!--JavaScript-->
<script src="<?= base_url('public/') ?>js/jquery.min.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>js/menu.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>js/ddsmoothmenu.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>js/drop-down.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>js/jquery.nestedAccordion.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>js/bookmarkscroll.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>js/left-menu.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>js/tab.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>js/jquery.jcarousel.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function() {
        if($(window).width() <560 ){
            $(".cart-top").insertAfter(".ddsmoothmenu");
        }
    });
</script>
<script type="text/javascript">
    function mycarousel_initCallback(carousel)
    {
        // Pause autoscrolling if the user moves with the cursor over the clip.
        carousel.clip.hover(function() {
            carousel.stopAuto();
        }, function() {
            carousel.startAuto();
        });
    };

    jQuery(document).ready(function()
    {
        // Initialise the first and second carousel by class selector.
        // Note that they use both the same configuration options (none in this case).
        jQuery('.d-carousel-thumb .carousel').jcarousel({
            scroll: 1,
            auto:false,
            loop: 1,
            wrap: false,
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $(".carousel a").click(function(e) {
            e.preventDefault();
            $("#View_image").attr("src", $(this).attr("href"));
            $("a").removeClass("active");
            $(this).addClass("active");
        });

        $('.product_details_form').on('submit', function (event)
        {
            event.preventDefault();
            event.stopPropagation();
            var $form = $(this);
            var form_data = new FormData($(this)[0]);
            $.ajax({
                url: $form.attr('action'),
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function(response)
                {
                    if(response.result == 1) {
                        $('.cart-top span').html('').append(response.data);
                        $('.message-response').append( '<span class="send">This item is successfully added.</span>' );
                        setTimeout(function(){$('.btn-wrap span').fadeOut();}, 1500);
                    }
                },
                error: function()
                {
                    jQuery( '.enquiry_cart' ).append( '<span class="error">This item was not successfully added.</span>' );
                    setTimeout(function(){jQuery('.enquiry_cart span').fadeOut();}, 1500);
                }
            });
        });
    });
</script>