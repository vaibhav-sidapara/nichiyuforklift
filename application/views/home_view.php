<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="banner-wrap">
    <div class="banner">
        <div class="flexslider">
            <ul class="slides">
                <li>
                    <img src="<?=base_url('public/')?>images/banner-01.jpg" alt="">
                    <div class="banner-caption">
                        <div class="container">
                            <article>Our quality electric forklifts
                                incorporate expert logistics
                                technologies for a better world.
                            </article>
                            <a href="Services-Support.html">LEARN MORE</a>
                        </div>
                    </div>
                </li>
                <li>
                    <img src="<?=base_url('public/')?>images/banner-02.jpg" alt="">
                    <div class="banner-caption">
                        <div class="container">
                            <article>Our quality electric forklifts
                                incorporate expert logistics
                                technologies for a better world.
                            </article>
                            <a href="Services-Support.html">LEARN MORE</a>
                        </div>
                    </div>
                </li>
                <li>
                    <img src="<?=base_url('public/')?>images/banner-03.jpg" alt="">
                    <div class="banner-caption">
                        <div class="container">
                            <article>Our quality electric forklifts
                                incorporate expert logistics
                                technologies for a better world.
                            </article>
                            <a href="Services-Support.html">LEARN MORE</a>
                        </div>
                    </div>
                </li>
                <li>
                    <img src="<?=base_url('public/')?>images/banner-04.jpg" alt="">
                    <div class="banner-caption">
                        <div class="container">
                            <article>Our quality electric forklifts
                                incorporate expert logistics
                                technologies for a better world.
                            </article>
                            <a href="Services-Support.html">LEARN MORE</a>
                        </div>
                    </div></li>
                <li>
                    <img src="<?=base_url('public/')?>images/banner-05.jpg" alt="">
                    <div class="banner-caption">
                        <div class="container">
                            <article>Our quality electric forklifts
                                incorporate expert logistics
                                technologies for a better world.
                            </article>
                            <a href="Services-Support.html">LEARN MORE</a>
                        </div>
                    </div>
                </li>
                <li>
                    <img src="<?=base_url('public/')?>images/banner-06.jpg" alt="">
                    <div class="banner-caption">
                        <div class="container">
                            <article>Our quality electric forklifts
                                incorporate expert logistics
                                technologies for a better world.
                            </article>
                            <a href="Services-Support.html">LEARN MORE</a>
                        </div>
                    </div>
                </li>
                <li>
                    <img src="<?=base_url('public/')?>images/banner-07.jpg" alt="">
                    <div class="banner-caption">
                        <div class="container">
                            <article>Our quality electric forklifts
                                incorporate expert logistics
                                technologies for a better world.
                            </article>
                            <a href="Services-Support.html">LEARN MORE</a>
                        </div>
                    </div>
                </li>
                <li>
                    <img src="<?=base_url('public/')?>images/banner-08.jpg" alt="">
                    <div class="banner-caption">
                        <div class="container">
                            <article>Our quality electric forklifts
                                incorporate expert logistics
                                technologies for a better world.
                            </article>
                            <a href="Services-Support.html">LEARN MORE</a>
                        </div>
                    </div>
                </li>
                <li>
                    <img src="<?=base_url('public/')?>images/banner-09.jpg" alt="">
                    <div class="banner-caption">
                        <div class="container">
                            <article>Our quality electric forklifts
                                incorporate expert logistics
                                technologies for a better world.
                            </article>
                            <a href="Services-Support.html">LEARN MORE</a>
                        </div>
                    </div>
                </li>
                <li>
                    <img src="<?=base_url('public/')?>images/banner-10.jpg" alt="">
                    <div class="banner-caption">
                        <div class="container">
                            <article>Our quality electric forklifts
                                incorporate expert logistics
                                technologies for a better world.
                            </article>
                            <a href="Services-Support.html">LEARN MORE</a>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>

<section class="welcome">
    <div class="container">
        <h2>Welcome!</h2>
        <p>Nichiyu Forklift Trucks designed to be environmentally friendly by providing energy saving machinery and equipment that limits carbon dioxide emissions. We are contributing to the creation of a prosperous and eco-friendly society.</p>
    </div>
</section>

<div class="body-wrap">

    <!--Top Products-->
    <div class="container">
        <div class="product-overview">
            <hgroup>
                <h2>Top Rated Products</h2>
                <h3>Nullam sit amet odio eu est aliquet euismod a a urna. Proin eu urna suscipit, dictum quam nec.</h3>
            </hgroup>
            <div class="top-Rated-product clearfix">
                <div class="product-box">
                    <figure>
                        <img src="<?=base_url('public/')?>images/pro-img-01.jpg" alt="">
                        <figcaption>
                            <h2>Sit-on</h2>
                            <h3>Reach Trucks</h3>
                            <a href="detail.html" class="view">VIEW</a>
                        </figcaption>
                    </figure>
                    <article>
                        <h2>Sit-on</h2>
                        <h3>Reach Trucks</h3>
                        <a href="detail.html" class="view">VIEW</a>
                    </article>
                </div>
                <div class="product-box">
                    <figure>
                        <img src="<?=base_url('public/')?>images/pro-img-02.jpg" alt="">
                        <figcaption>
                            <h2>Low Profile</h2>
                            <h3>Counterbalanced Trucks</h3>
                            <a href="detail.html" class="view">VIEW</a>
                        </figcaption>
                    </figure>
                    <article>
                        <h2>Low Profile</h2>
                        <h3>Counterbalanced Trucks</h3>
                        <a href="detail.html" class="view">VIEW</a>
                    </article>
                </div>
                <div class="product-box">
                    <figure>
                        <img src="<?=base_url('public/')?>images/pro-img-03.jpg" alt="">
                        <figcaption>
                            <h2>Pallet Picker</h2>
                            <h3>Rack Forklift Trucks</h3>
                            <a href="detail.html" class="view">VIEW</a>
                        </figcaption>

                    </figure>
                    <article>
                        <h2>Pallet Picker</h2>
                        <h3>Rack Forklift Trucks</h3>
                        <a href="detail.html" class="view">VIEW</a>
                    </article>
                </div>
                <div class="product-box">
                    <figure>
                        <img src="<?=base_url('public/')?>images/pro-img-04.jpg" alt="">
                        <figcaption>
                            <h2>Three Wheeler</h2>
                            <h3>Towing Tractors</h3>
                            <a href="detail.html" class="view">VIEW</a>
                        </figcaption>

                    </figure>
                    <article>
                        <h2>Three Wheeler</h2>
                        <h3>Towing Tractors</h3>
                        <a href="detail.html" class="view">VIEW</a>
                    </article>
                </div>
                <div class="product-box">
                    <figure>
                        <img src="<?=base_url('public/')?>images/pro-img-05.jpg" alt="">
                        <figcaption>
                            <h2>Counterbalanced Trucks</h2>
                            <h3>Cold Storage Type</h3>
                            <a href="detail.html" class="view">VIEW</a>
                        </figcaption>

                    </figure>
                    <article>
                        <h2>Counterbalanced Trucks</h2>
                        <h3>Cold Storage Type</h3>
                        <a href="detail.html" class="view">VIEW</a>
                    </article>
                </div>
                <div class="product-box">
                    <figure>
                        <img src="<?=base_url('public/')?>images/pro-img-06.jpg" alt="">
                        <figcaption>
                            <h2>Mini</h2>
                            <h3>Counterbalanced Trucks</h3>
                            <a href="detail.html" class="view">VIEW</a>
                        </figcaption>

                    </figure>
                    <article>
                        <h2>Mini</h2>
                        <h3>Counterbalanced Trucks</h3>
                        <a href="detail.html" class="view">VIEW</a>
                    </article>
                </div>
            </div>
            <div class="link-wrap">
                <a href="Products.html" class="view-all">VIEW ALL CATEGORIES</a>
            </div>
        </div>
    </div>

    <!--Latest News-->
    <div class="latest-news">
        <hgroup>
            <h2>Latest News</h2>
            <h3>Nullam sit amet odio eu est aliquet euismod a a urna. Proin eu urna suscipit, dictum quam nec. </h3>
        </hgroup>
        <div class="news-carusel">
            <div class="d-carousel">
                <ul class="carousel">
                    <li>
                        <div class="news-block clearfix">
                            <div class="news-text">
                                <div class="news-top"><h2>NAS ACEANIA Regional
                                        Distibutions Conference 2014</h2>
                                    <span class="date">February 2014</span></div>
                                <div class="news-info"><article>NAS recently concluded 2014 Distributors conference in Kyoto, Japan. Top senior management staffs from Nichiyu Mitsubishi Japan graced the event held in Kyoto  office from 27th – 28th April...</article>
                                    <a href="news-detail.html" class="readmore">READ MORE</a></div>
                            </div>
                            <figure>
                                <img src="<?=base_url('public/')?>images/news-01.jpg" alt="">
                            </figure>
                        </div>
                    </li>
                    <li>
                        <div class="news-block clearfix">
                            <div class="news-text">
                                <div class="news-top"><h2>The Second Asia Nichiyu Service Skills Contest</h2>
                                    <span class="date">September 2013</span></div>
                                <div class="news-info"><article>The Second Asia NICHIYU Service Skills Contest, organized by Mitsubishi Nichiyu Forklift Co.,Ltd and co-organized by Nichiyu Asia (Thailand) Co.,Ltd, was successfully held on 6 September 2013...</article>
                                    <a href="news-detail.html" class="readmore">READ MORE</a></div>
                            </div>
                            <figure>
                                <img src="<?=base_url('public/')?>images/news-02.jpg" alt="">
                            </figure>
                        </div>
                    </li>
                    <li>
                        <div class="news-block clearfix">
                            <div class="news-text">
                                <div class="news-top"><h2>3rd ACEANIA Nichiyu Serviceman Contest 2013
                                    </h2>
                                    <span class="date">July 2013</span></div>
                                <div class="news-info"><article>The 3rd ACEANIA Nichiyu Serviceman Contest organized by Nichiyu Asia Pte. Ltd. and co-organized by Nichiyu Asia (Thailand) Co., Ltd. was successfully held on 25th July 2013. The venue of the...</article>
                                    <a href="news-detail.html" class="readmore">READ MORE</a>
                                </div>
                            </div>
                            <figure>
                                <img src="<?=base_url('public/')?>images/news-03.jpg" alt="">
                            </figure>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="link-wrap">
            <a href="Latest-News.html" class="view-all">VIEW ALL NEWS</a>
        </div>
    </div>

    <!--Achievements-->
    <div class="home-Achievements gallery" id="gallery" >
        <div class="container">
            <hgroup>
                <h2>Achievements</h2>
                <h3>Nullam sit amet odio eu est aliquet euismod a a urna. Proin eu urna suscipit, dictum quam nec. </h3>
            </hgroup>
            <div class="carusel-Achievements" >
                <div class="d-carousel2">
                    <ul class="carousel">
                        <li>
                            <div class="Achievements">
                                <figure>
                                    <figcaption>
                                        <a class="view" href="<?=base_url('public/')?>images/achive-01.jpg" rel="prettyPhoto[gallery1]" title="" >VIEW</a>
                                    </figcaption>
                                    <img src="<?=base_url('public/')?>images/achive-01.jpg" alt="">
                                </figure>
                                <article>Nichiyu Wins Good Design Award 2012 for Three-wheeler Counterbalanced Truck</article>
                            </div>
                        </li>
                        <li>
                            <div class="Achievements">
                                <figure>
                                    <figcaption><a class="view" href="images/achive-02.jpg" rel="prettyPhoto[gallery2]" title="">VIEW</a></figcaption>
                                    <img src="<?=base_url('public/')?>images/achive-02.jpg" alt="">
                                </figure>
                                <article>NICHIYU updates explosion-proof
                                    counterbalance truck : FB-E70</article>
                            </div>
                        </li>
                        <li>
                            <div class="Achievements">
                                <figure>
                                    <figcaption><a class="view" href="<?=base_url('public/')?>images/achive-03.jpg" rel="prettyPhoto[gallery3]" title="">VIEW</a></figcaption>
                                    <img src="<?=base_url('public/')?>images/achive-03.jpg" alt="">
                                </figure>
                                <article>Nichiyu introduces updated Rack Forklift</article>
                            </div>
                        </li>
                        <li>
                            <div class="Achievements">
                                <figure>
                                    <figcaption><a class="view" href="<?=base_url('public/')?>images/achive-03.jpg" rel="prettyPhoto[gallery4]" title="">VIEW</a></figcaption>

                                    <img src="<?=base_url('public/')?>images/achive-03.jpg" alt="">
                                </figure>
                                <article>Nichiyu introduces updated Rack Forklift</article>
                            </div>
                        </li>
                        <li>
                            <div class="Achievements">
                                <figure>
                                    <figcaption><a class="view" href="<?=base_url('public/')?>images/achive-03.jpg" rel="prettyPhoto[gallery5]" title="">VIEW</a></figcaption>

                                    <img src="<?=base_url('public/')?>images/achive-03.jpg" alt="">
                                </figure>
                                <article>Nichiyu introduces updated Rack Forklift</article>
                            </div>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="locator">
        <div class="container">
            <span class="style-01">Quick Dealer Locator</span>
            <div class="select-location ">
                <a class="popup-with-form" href="#test-form">Open form</a>
                <select title="">
                    <option >Select your country</option>
                    <option >Australia</option>
                    <option >India</option>
                    <option >Indonesia</option>
                    <option >Korea</option>
                    <option >Malaysia</option>
                    <option >Pakistan</option>
                    <option >Philippines</option>
                    <option >South Africa</option>
                    <option >Singapore</option>
                    <option >Sri Lanka</option>
                    <option >Taiwan</option>
                    <option >Thailand</option>
                    <option >Vietnam</option>
                </select>
            </div>
        </div>
    </div>
</div>

<div id="test-form" class="mfp-hide white-popup-block">
    <h2 class="popup-title">Quick Dealer Locator</h2>
    <div class="content mCustomScrollbar">
        <div class="locket-wrap">
            <div class="row-location">
                <div class="field-row clearfix">
                    <span class="field">Dealer Name</span>Michael Ng
                </div>
                <div class="field-row clearfix">
                    <span class="field">Address</span>250 N Bridge Rd, #06-00 Raffles City Tower
                </div>
                <div class="field-row clearfix">
                    <span class="field">Phone</span>+65 1234 5678
                </div>
            </div>
            <div class="row-location">
                <div class="field-row clearfix">
                    <span class="field">Dealer Name</span>Michael Ng
                </div>
                <div class="field-row clearfix">
                    <span class="field">Address</span>250 N Bridge Rd, #06-00 Raffles City Tower
                </div>
                <div class="field-row clearfix">
                    <span class="field">Phone</span>+65 1234 5678
                </div>
            </div>
            <div class="row-location">
                <div class="field-row clearfix">
                    <span class="field">Dealer Name</span>Michael Ng
                </div>
                <div class="field-row clearfix">
                    <span class="field">Address</span>250 N Bridge Rd, #06-00 Raffles City Tower
                </div>
                <div class="field-row clearfix">
                    <span class="field">Phone</span>+65 1234 5678
                </div>
            </div>
        </div>

    </div>
</div>


<!--JavaScript-->
<script src="<?=base_url('public/')?>js/jquery.min.js" type="text/javascript"></script>
<!--Nav Menu-->
<script src="<?=base_url('public/')?>js/menu.js" type="text/javascript"></script>
<script src="<?=base_url('public/')?>js/ddsmoothmenu.js" type="text/javascript"></script>
<script src="<?=base_url('public/')?>js/drop-down.js" type="text/javascript"></script>
<script src="<?=base_url('public/')?>js/jquery.prettyPhoto.js" type="text/javascript"></script>
<script src="<?=base_url('public/')?>js/jquery.prettyPhoto.js" type="text/javascript"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function(){
        $(".gallery:first a[rel^='prettyPhoto']").prettyPhoto({
            animationSpeed:'slow',
            theme:'facebook',
            slideshow:2000,
            autoplay_slideshow: false,
            show_title: false
        });

        $(".contactDetails a[rel^='prettyPhoto']:first").prettyPhoto({
            custom_markup: '<div id="map_canvas" style="width:400px; height:400px"></div>',
            theme:'light_rounded',
            changepicturecallback: function(){ initialize(); }
        });
    });
</script>

<script type="text/javascript">
    $(document).ready(function() {
        $('.popup-with-form').magnificPopup({
            type: 'inline',
            preloader: false,
            focus: '#name',

            // When elemened is focused, some mobile browsers in some cases zoom in
            // It looks not nice, so we disable it:
            callbacks: {
                beforeOpen: function() {
                    if($(window).width() < 700) {
                        this.st.focus = false;
                    } else {
                        this.st.focus = '#name';
                    }
                }
            }
        });
    });
</script>

<link rel="stylesheet" href="<?=base_url('public/')?>css/carusal-product.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?=base_url('public/')?>css/Achievements.css" type="text/css" media="screen" />
<script type="text/javascript" src="<?=base_url('public/')?>js/jquery.jcarousel.js"></script>
<script type="text/javascript">


    jQuery(document).ready(function() {
        // Initialise the first and second carousel by class selector.
        // Note that they use both the same configuration options (none in this case).
        jQuery('.d-carousel .carousel').jcarousel({
            scroll: 1,
            auto:5,
            loop: 1,
            animation:800
        });
        jQuery('.d-carousel2 .carousel').jcarousel({
            scroll: 1,
            auto:false,
            loop:5,
        });
    });
</script>

<script type="application/javascript" defer src="<?=base_url('public/')?>js/jquery.flexslider.js"></script>
<script type="text/javascript">
    $(function(){
        SyntaxHighlighter.all();
    });
    $(window).load(function(){
        $('.flexslider').flexslider({
            animation: "fade",
            direction: "horizontal",
            controlNav:false,
            directionNav: true,
            start: function(slider){
                $('body').removeClass('loading');
            }
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        if($(window).width() <560 ){
            $(".cart-top").insertAfter(".ddsmoothmenu");
        }
    });
</script>
<script type="application/javascript" src="<?=base_url('public/')?>js/jquery.magnific-popup.min.js"></script>
<script type="application/javascript" src="<?=base_url('public/')?>js/jquery.mCustomScrollbar.concat.min.js"></script>
