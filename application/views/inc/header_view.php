<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
    <meta charset="utf-8">
    <title><?=$title?></title>
    <meta name="description" content="Contrary to popular belief, Lorem Ipsum is not simply random text." />
    <meta name="keywords" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="<?=base_url('public/')?>favicon/favicon.ico"/>
    <link rel="icon" type="image/gif" href="<?=base_url('public/')?>favicon/favicon.gif"/>

    <link href="<?=base_url('public/')?>css/style.css" rel="stylesheet" type="text/css">
    <link href="<?=base_url('public/')?>css/fonts.css" rel="stylesheet" type="text/css">
    <link href="<?=base_url('public/')?>css/reset.css" rel="stylesheet" type="text/css">
    <link href="<?=base_url('public/')?>css/responsive.css" rel="stylesheet" type="text/css">
    <link href="<?=base_url('public/')?>css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="<?=base_url('public/')?>css/mobile-menu.css" rel="stylesheet" type="text/css">
    <link href="<?=base_url('public/')?>css/ddsmoothmenu.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url('public/')?>css/prettyPhoto.css" rel="stylesheet" type="text/css">
    <link href="<?=base_url('public/')?>css/animate.css" rel="stylesheet" type="text/css"/>
    <link href="<?=base_url('public/')?>css/flexslider.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?=base_url('public/')?>css/acrodian-content.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url('public/')?>css/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url('public/')?>css/all.min.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url('public/')?>css/left-menu.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url('public/')?>css/carusal_thumb.css" rel="stylesheet" type="text/css" media="screen" />


    <!--[if IE]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js" type="text/javascript"></script>
    <![endif]-->
</head>

<body>

<div class="Over_flow">
    <header class="header-wrap">
        <div class="head-top">
            <div class="container">
                <aside class="top-search clearfix">
                    <form class="search" action="<?=base_url('search')?>" method="POST">
                    <div class="General-Search">
                        <select title="search-option" name="search-option">
                            <option value="general">General Search</option>
                            <option value="category">Category Search</option>
                            <option value="sub-category">Sub Category Search</option>
                            <option value="product">Product Search</option>
                        </select>
                    </div>
                    <div class="Keyword-search">
                        <input title="keyword" type="text" value="Type Keyword" onBlur="if(this.value=='')this.value='Type Keyword';" onFocus="if(this.value=='Type Keyword')this.value='';" name="keyword">
                        <button type="submit">Search</button>
                    </div>
                    </form>
                </aside>
                <div class="cart-top">
                    <a href="<?=base_url('enquiry-cart')?>">Enquiry Cart<i class="fa fa-cart-arrow-down"></i></a>
                    <span><?=count($this->session->userdata('cart'))?></span>
                </div>
            </div>
        </div>

        <!--Navigation-->
        <div class="header">
            <div class="container">
                <figure>
                    <a href="<?=base_url()?>"><img src="<?=base_url('public/')?>images/logo.png" alt=""></a>
                </figure>
                <div class="nav-wrap clearfix">
                    <nav class="ddsmoothmenu" id="smoothmenu1">
                        <ul>
                            <li class="<?=activate_method('welcome');?>"><a href="<?=base_url('')?>">Home</a></li>
                            <li class="<?=activate_controller('products');?>"><a href="<?=base_url('products')?>">Products</a>
                                <ul>
                                    <?php
                                    foreach ($this->session->userdata('categories_structure') as $category)
                                    {
                                        echo "<li><a href=\"". base_url('products/category/'.$category->slug) ."\">$category->name</a>";
                                            if(!empty($category->sub_category)) {
                                                echo "<ul>";
                                                foreach ($category->sub_category as $sub)  {
                                                    echo "<li><a href=\"". base_url('products/sub-category/'.$sub->slug) ."\">$sub->name</a></li>";
                                                }
                                                echo "</ul>";
                                            }
                                        echo "</li>";
                                    }
                                    ?>
                                </ul>
                            </li>
                            <li class="<?=activate_method('services_support');?>"><a href="<?=base_url('services-support')?>">Services &amp; Support</a></li>
                            <li class="<?=activate_method('dealer_locator');?>"><a href="<?=base_url('dealer-locator')?>">Dealer Locator</a></li>
                            <li class="<?=activate_controller('news');?>"><a href="<?=base_url('latest-news')?>">Latest News</a></li>
                            <li class="<?=activate_method('contact_us');?>"><a href="<?=base_url('contact-us')?>">Contact Us</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        <div class="shado"></div>
    </header>

    <?php if( isset($banner_image) ) { ?>
        <div class="banner-wrap">
            <img src="<?= base_url('public/') ?>images/<?=$banner_image?>" alt="" class="imageResponsive">
        </div>
    <?php } ?>
