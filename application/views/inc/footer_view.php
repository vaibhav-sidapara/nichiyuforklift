<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<footer class="footer-wrap">
    <div class="container">
        <small>Copyright &copy; <?=date('Y')?> All rights reserved. All registered trademarks are the property of their respective owners.</small>
        <aside>
            <a href="http://www.verzdesign.com/"  target="_blank">Web Design</a> by
            <a href="http://www.verzdesign.com/"  target="_blank"><strong>Verz</strong><img src="<?=base_url('public/')?>images/verz.png" alt=""></a>
        </aside>
    </div>
</footer>

</div>

</body>