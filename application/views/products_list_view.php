<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php
foreach ($this->session->userdata('categories_structure') as $category) { ?>
    <li <?=($category->slug == $active_slug)?"class=\"Sub active\"":""?>><span><a href="<?=base_url('products/category/'.$category->slug)?>"><?=$category->name?></a></span>
        <?php if(!empty($category->sub_category)) { ?>
            <ul>
                <?php foreach ($category->sub_category as $sub)  {
                    echo "<li><span><a href=\"". base_url('products/sub-category/'.$sub->slug) ."\">$sub->name</a></span></li>";
                } ?>
            </ul>
        <?php } ?>
    </li>
<?php } ?>