<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="page-with-banner">
    <div class="container">
        <pre>

        </pre>
        <div class="left-menu">
            <h2>In The News<i class="click"></i><i class="normal-icon"></i></h2>
            <div class="menu">
                <ul id="accordion">
                    <?php foreach($news_archive_list as $year => $months) { ?>
                        <li <?=($year == $year_active)?"class=\"Sub active\"":"class=\"Sub\"" ?>><span><a href="javascript:void(0)">Year <?=$year?></a></span>
                            <ul <?=($year == $year_active)?"class=\"selected\"":"" ?>>
                                <?php foreach($months as $month) { ?>
                                    <li <?=($month['month'] == $month_active)?"class=\"active\"":"" ?>><a href="<?=base_url('latest-news/').$year.'/'.$month['month_num']?>"><?=$month['month']?></a></li>
                                <?php } ?>
                            </ul>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
        <div class="right-col">
            <div class="bred-camb">
                <a href="<?=base_url('')?>">Home</a>
                <span class="seprster fa fa-angle-right"></span>
                <a href="Latest-News.html">Latest News</a>
                <?php if(isset($month_for_breadcrumb)) { ?>
                <span class="seprster fa fa-angle-right"></span>
                <?=$month_for_breadcrumb?>
                <?php } ?>
            </div>
            <h2 class="page-title">Latest News</h2>

            <?php foreach($news_archive as $news) {?>
            <div class="news-row clearfix">
                <figure><img src="<?= base_url('public/') ?>images/img-news-01.jpg" alt=""></figure>
                <div class="news-info">
                    <h2><?=$news['name']?></h2>
                    <span class="date"><?=$news['month']." ".$news['year'];?></span>
                    <article>
                        <?=$news['description']?>
                    </article>
                </div>
                <div class="clear"></div>
                <a href="<?=base_url('latest-news/details/').$news['slug']?>" class="readmore">read more</a>
            </div>
            <?php } ?>

            <div class="pagin-bottom clearfix">
                <div class="pagen-wrap pagein">
                    <ul>
                        <li class="pre"><a href="#"></a></li>
                        <li><a href="#" class="active">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li class="nex"><a href="#"></a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>
</div>


<!--JavaScript-->
<script src="<?= base_url('public/') ?>js/jquery.min.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>js/menu.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>js/ddsmoothmenu.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>js/drop-down.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>js/jquery.nestedAccordion.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>js/bookmarkscroll.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>js/left-menu.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function() {
        if($(window).width() <560 ){
            $(".cart-top").insertAfter(".ddsmoothmenu");
        }
    });
</script>

<script type="text/javascript">
    $("#accordion > li > span").click(function(){

        if(false == $(this).next().is(':visible')) {
            $('#accordion ul').slideUp(300);
        }
        $(this).next().slideToggle(300);
    });

    $(".click").click(function(){
        $(".menu").slideToggle(300);
    });


    //$('#accordion ul:eq(0)').show();

</script>
<script type="text/javascript">
    $(document).ready(function(){
        $(".Sub span").click(function(e) {
            e.preventDefault();
            $("span").removeClass("active-Sub");
            $(this).addClass("active-Sub");
        })
    });
</script>