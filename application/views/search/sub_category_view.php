<?php foreach ($sub_categories as $category) { ?>
    <div class="news-row clearfix">
        <figure><a href="<?=base_url('products/sub-category/').$category['slug']?>"><img alt="" src="<?=base_url('public/').$category['image']?>"></a></figure>
        <div class="news-info">
            <h2><a href="<?=base_url('products/sub-category/').$category['slug']?>"><?=$category['name']?></a></h2>
        </div>
        <div class="clear"></div><a class="readmore" href="<?=base_url('products/sub-category/').$category['slug']?>">View Details</a>
    </div>
<?php } ?>