<?php foreach($news_archive as $news) {?>
    <div class="news-row clearfix">
        <figure><img src="<?= base_url('public/') ?>images/img-news-01.jpg" alt=""></figure>
        <div class="news-info">
            <h2><?=$news['name']?></h2>
            <span class="date"><?=$news['month']." ".$news['year'];?></span>
            <article>
                <?=$news['description']?>
            </article>
        </div>
        <div class="clear"></div>
        <a href="<?=base_url('latest-news/details/').$news['slug']?>" class="readmore">read more</a>
    </div>
<?php } ?>