<?php foreach ($products as $product) { ?>
    <div class="news-row clearfix">
        <figure><a href="<?=base_url('products/details/').$product['slug']?>"><img alt="" src="<?=base_url('public/').$product['image']?>"></a></figure>
        <div class="news-info">
            <h2><a href="<?=base_url('products/details/').$product['slug']?>"><?=$product['name']?></a></h2>
        </div>
        <div class="clear"></div><a class="readmore" href="<?=base_url('products/details/').$product['slug']?>">View Details</a>
    </div>
<?php } ?>