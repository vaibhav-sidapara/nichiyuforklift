<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="page-with-banner thankyou-page">
    <div class="container">
        <div class="bred-camb"><a href="<?=base_url('')?>">Home</a><span class="seprster fa fa-angle-right"></span>Contact Us</div>
        <div class="thankyou">
            <h2>Thank You!</h2>
            <h3>THANK YOU FOR YOUR ENQUIRY</h3>
            <article>We have received your questions/feedback, and dropped you a confirmation email as well.</article>
            <div class="thankyou-footer">
                <aside>Meanwhile, you may want to find out more about Nichiyu Electric Forklift below:</aside>
                <ul>
                    <li><a href="<?=base_url('products')?>">Products</a></li>
                    <li><a href="<?=base_url('services-support')?>">Services &amp; Support</a></li>
                    <li><a href="<?=base_url('dealer-locator')?>">Dealer Locator</a></li>
                    <li><a href="<?=base_url('latest-news')?>">Latest News</a></li>
                </ul>
            </div>
        </div>
        <img src="<?=base_url('public/')?>images/thankyou.jpg" alt="" class="image-thankyou">
    </div>
</div>

<!--JavaScript-->
<script src="<?= base_url('public/') ?>js/jquery.min.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>js/menu.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>js/ddsmoothmenu.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>js/drop-down.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>js/jquery.nestedAccordion.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function() {
        if($(window).width() <560 ){
            $(".cart-top").insertAfter(".ddsmoothmenu");
        }
    });
</script>