<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container">
    <div class="bred-camb"><a href="http://www.nichiyuforklift.com.sg/">Home</a><span class="seprster fa fa-angle-right"></span>Search</div>
    <h2 class="page-title">General Search Results: <?=$search_keyword?> </h2>

    <div class="listing_wrap">
        <?php if(!empty($news_archive)) { $this->view('search/general_view', ['news_archive' => $news_archive]); } ?>
        <?php if(!empty($categories)) { $this->view('search/category_view'); ['categories' => $categories];} ?>
        <?php if(!empty($sub_categories)) { $this->view('search/sub_category_view'); ['sub_categories' => $sub_categories];} ?>
        <?php if(!empty($products)) { $this->view('search/product_view'); ['products' => $products];} ?>
        <?php if( empty($news_archive) && empty($categories) && empty($sub_categories) && empty($products) ) { ?>
            <h2 class="">No Results Found</h2>
            <div class="clear"><br/></div>
        <?php } ?>

    </div>
</div>

<!--JavaScript-->
<script src="<?= base_url('public/') ?>js/jquery.min.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>js/menu.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>js/ddsmoothmenu.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>js/drop-down.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>js/jquery.nestedAccordion.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function() {
        if($(window).width() <560 ){
            $(".cart-top").insertAfter(".ddsmoothmenu");
        }
    });
</script>