<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="page-with-banner">
    <div class="container">
        <div class="bred-camb">
            <a href="<?=base_url('')?>">Home</a><span class="seprster fa fa-angle-right"></span>Services &amp; Support
        </div>
        <h2 class="page-title">Services &amp; Support</h2>
        <div class="service" id="acco">

            <div class="accordion demo" id="acc1">
                <header><a href="#">
                        <span class="menue_title">Providing Our Distributors with Efficient Parts Supply</span>
                    </a>
                </header>
                <div class="inner clearfix">
                    <p> Although Nichiyu electric forklift trucks are built to last, regular maintenance is essential for optimal performance and reduced possibility of breakdown. Our spare parts warehouse in Singapore provides an
                        <strong>efficient and reliable line of parts supply</strong> to our distributors in South East Asia, South Asia and Oceania. Our experience staff and efficient parts management system allow us to regularly achieve
                        <strong>more than 80% order fulfillment.</strong></p>

                    <p>We understand the importance of parts availability, and are committed to supplying you with quality Nichiyu
                        <strong>genuine spare parts</strong>. You will not have to chase around for spare parts or suffer from bottlenecks in your operation due to truck breakdowns. Investing in regular maintenance and using genuine Nichiyu parts will, in the long run, reduce your servicing needs, optimize your truck performance, and ultimately increase your bottom line. All genuine Nichiyu spare parts come with
                        <strong>six months warranty</strong> from defects in workmanship or material.</p>

                    <p>In addition to providing you with the right parts at the right price and right on time, we also have
                        <strong>technical advisors from Japan stationed in Singapore and Thailand</strong>. They provide
                        <strong>product training and support</strong> for our distributors, and are always available to assist in resolving complex technical issues. You can also rest assure that all our distributors’ technicians are professional, well-trained, and customer oriented.
                    </p>

                    <p>Please do not hesitate to
                        <a href="<?=base_url('contact-us')?>">contact us</a> if you need any professional advice or have any enquiries with regards to Nichiyu spare parts.
                    </p>

                </div>
                <header><a href="#">
                        <span class="menue_title">MAECENAS MOLESTIE NON</span>
                    </a>
                </header>
                <div class="inner clearfix">
                    <p> Although Nichiyu electric forklift trucks are built to last, regular maintenance is essential for optimal performance and reduced possibility of breakdown. Our spare parts warehouse in Singapore provides an
                        <strong>efficient and reliable line of parts supply</strong> to our distributors in South East Asia, South Asia and Oceania. Our experience staff and efficient parts management system allow us to regularly achieve
                        <strong>more than 80% order fulfillment.</strong></p>

                    <p>We understand the importance of parts availability, and are committed to supplying you with quality Nichiyu
                        <strong>genuine spare parts</strong>. You will not have to chase around for spare parts or suffer from bottlenecks in your operation due to truck breakdowns. Investing in regular maintenance and using genuine Nichiyu parts will, in the long run, reduce your servicing needs, optimize your truck performance, and ultimately increase your bottom line. All genuine Nichiyu spare parts come with
                        <strong>six months warranty</strong> from defects in workmanship or material.</p>

                    <p>In addition to providing you with the right parts at the right price and right on time, we also have
                        <strong>technical advisors from Japan stationed in Singapore and Thailand</strong>. They provide
                        <strong>product training and support</strong> for our distributors, and are always available to assist in resolving complex technical issues. You can also rest assure that all our distributors’ technicians are professional, well-trained, and customer oriented.
                    </p>

                    <p>Please do not hesitate to
                        <a href="<?=base_url('contact-us')?>">contact us</a> if you need any professional advice or have any enquiries with regards to Nichiyu spare parts.
                    </p>

                </div>
                <header><a href="#">
                        <span class="menue_title">dUI PORTTITOR NULLA</span>
                    </a>
                </header>
                <div class="inner clearfix">
                    <p> Although Nichiyu electric forklift trucks are built to last, regular maintenance is essential for optimal performance and reduced possibility of breakdown. Our spare parts warehouse in Singapore provides an
                        <strong>efficient and reliable line of parts supply</strong> to our distributors in South East Asia, South Asia and Oceania. Our experience staff and efficient parts management system allow us to regularly achieve
                        <strong>more than 80% order fulfillment.</strong></p>

                    <p>We understand the importance of parts availability, and are committed to supplying you with quality Nichiyu
                        <strong>genuine spare parts</strong>. You will not have to chase around for spare parts or suffer from bottlenecks in your operation due to truck breakdowns. Investing in regular maintenance and using genuine Nichiyu parts will, in the long run, reduce your servicing needs, optimize your truck performance, and ultimately increase your bottom line. All genuine Nichiyu spare parts come with
                        <strong>six months warranty</strong> from defects in workmanship or material.</p>

                    <p>In addition to providing you with the right parts at the right price and right on time, we also have
                        <strong>technical advisors from Japan stationed in Singapore and Thailand</strong>. They provide
                        <strong>product training and support</strong> for our distributors, and are always available to assist in resolving complex technical issues. You can also rest assure that all our distributors’ technicians are professional, well-trained, and customer oriented.
                    </p>

                    <p>Please do not hesitate to
                        <a href="<?=base_url('contact-us')?>">contact us</a> if you need any professional advice or have any enquiries with regards to Nichiyu spare parts.
                    </p>

                </div>
                <header><a href="#">
                        <span class="menue_title">VIVAMUS VITAE CONDIMENTUM</span>
                    </a>
                </header>
                <div class="inner clearfix">
                    <p> Although Nichiyu electric forklift trucks are built to last, regular maintenance is essential for optimal performance and reduced possibility of breakdown. Our spare parts warehouse in Singapore provides an
                        <strong>efficient and reliable line of parts supply</strong> to our distributors in South East Asia, South Asia and Oceania. Our experience staff and efficient parts management system allow us to regularly achieve
                        <strong>more than 80% order fulfillment.</strong></p>

                    <p>We understand the importance of parts availability, and are committed to supplying you with quality Nichiyu
                        <strong>genuine spare parts</strong>. You will not have to chase around for spare parts or suffer from bottlenecks in your operation due to truck breakdowns. Investing in regular maintenance and using genuine Nichiyu parts will, in the long run, reduce your servicing needs, optimize your truck performance, and ultimately increase your bottom line. All genuine Nichiyu spare parts come with
                        <strong>six months warranty</strong> from defects in workmanship or material.</p>

                    <p>In addition to providing you with the right parts at the right price and right on time, we also have
                        <strong>technical advisors from Japan stationed in Singapore and Thailand</strong>. They provide
                        <strong>product training and support</strong> for our distributors, and are always available to assist in resolving complex technical issues. You can also rest assure that all our distributors’ technicians are professional, well-trained, and customer oriented.
                    </p>

                    <p>Please do not hesitate to
                        <a href="<?=base_url('contact-us')?>">contact us</a> if you need any professional advice or have any enquiries with regards to Nichiyu spare parts.
                    </p>

                </div>
                <header><a href="#">
                        <span class="menue_title">ALIQUAM SCELERISQUE DOLOR</span>
                    </a>
                </header>
                <div class="inner clearfix">
                    <p> Although Nichiyu electric forklift trucks are built to last, regular maintenance is essential for optimal performance and reduced possibility of breakdown. Our spare parts warehouse in Singapore provides an
                        <strong>efficient and reliable line of parts supply</strong> to our distributors in South East Asia, South Asia and Oceania. Our experience staff and efficient parts management system allow us to regularly achieve
                        <strong>more than 80% order fulfillment.</strong></p>

                    <p>We understand the importance of parts availability, and are committed to supplying you with quality Nichiyu
                        <strong>genuine spare parts</strong>. You will not have to chase around for spare parts or suffer from bottlenecks in your operation due to truck breakdowns. Investing in regular maintenance and using genuine Nichiyu parts will, in the long run, reduce your servicing needs, optimize your truck performance, and ultimately increase your bottom line. All genuine Nichiyu spare parts come with
                        <strong>six months warranty</strong> from defects in workmanship or material.</p>

                    <p>In addition to providing you with the right parts at the right price and right on time, we also have
                        <strong>technical advisors from Japan stationed in Singapore and Thailand</strong>. They provide
                        <strong>product training and support</strong> for our distributors, and are always available to assist in resolving complex technical issues. You can also rest assure that all our distributors’ technicians are professional, well-trained, and customer oriented.
                    </p>

                    <p>Please do not hesitate to
                        <a href="<?=base_url('contact-us')?>">contact us</a> if you need any professional advice or have any enquiries with regards to Nichiyu spare parts.
                    </p>

                </div>
            </div>
        </div>
    </div>
</div>


<!--JavaScript-->
<script src="<?= base_url('public/') ?>js/jquery.min.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>js/menu.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>js/ddsmoothmenu.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>js/drop-down.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>js/jquery.nestedAccordion.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function() {
        if($(window).width() <560 ){
            $(".cart-top").insertAfter(".ddsmoothmenu");
        }
    });
</script>
<script type="text/javascript">
    <!--//--><![CDATA[//><!--
    $("html").addClass("js");
    $(function() {
        $("#acco").accordion({
            objID:"#acc1",
            obj:"div",
            wrapper:"div",
            el:".h",
            head:"header",
            next:"div",
            initShow : "div.outer:first",
            showMethod: "slideFadeDown",
            hideMethod: "slideFadeUp",
            scrollSpeed:100
        });

        $("html").removeClass("js");
    });
    //--><!]]>
</script>