<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="page-with-banner">
    <div class="clear_30"></div>
    <div class="container">
        <div class="bred-camb"><a href="<?=base_url('')?>">Home</a><span class="seprster fa fa-angle-right"></span>Enquiry Cart</div>
        <?php if( !empty($cart)) { ?>
        <h2 class="page-title">Enquiry Cart</h2>
        <div class="clear"></div>
        <?php foreach($cart as $product) {?>
        <div class="cart-row clearfix">
            <article class="Enquiry">
                <figure>
                    <img src="<?=base_url('public/').$product['image']?>" alt="">
                </figure>
                <h2><?=$product['name']?></h2>
                <div class="cart-info">
                    <strong><?=$product['capacity']?></strong>
                    <?=$product['description']?>
                </div>
                <div class="product-info">
                    <ul>
                        <li><span>Model:</span>
                            <div class="select-model">
                                <select class="model-select" title="model" name="model">
                                <?php
                                foreach($products_models[ $product['slug'] ] as $model) {
                                    echo ($model['name'] == $product['model'])? "<option selected='selected' data-foo='{$product['slug']}' value='{$model['name']}'>{$model['name']}</option>" : "<option data-foo='{$product['slug']}' value='{$model['name']}' >{$model['name']}</option>";
                                }
                                ?>
                                </select>
                            </div>
                        </li>
                    </ul>
                </div><span class="delete"><a href="<?=base_url('enquiry-cart/remove/').$product['slug']?>"><i class="fa fa-close"></i></a></span>
            </article>
        </div>
        <?php } ?>

        <div class="Enquire-Now">
            <h3>Enquire Now</h3>
            <div class="Enquire-form clearfix">
                <form class="enquiry-cart-form" method="POST">
                <div class="col-form clearfix">
                    <div class="form-row">
                        <label class="fleld">Name<span class="req">*</span></label>
                        <input title="name" type="text" name="name" value="">
                    </div>
                    <div class="form-row">
                        <label class="fleld">Company</label>
                        <input title="company" type="text" name="company" value="">
                    </div>
                    <div class="form-row">
                        <label class="fleld">Phone<span class="req">*</span></label>
                        <input title="tel" type="tel" name="tel" value="">
                    </div>
                    <div class="form-row">
                        <label class="fleld">Country<span class="req">*</span></label>
                        <select title="country" name="country">
                            <option>Select</option>
                            <option value="Afghanistan">
                                Afghanistan
                            </option>
                            <option value="Albania">
                                Albania
                            </option>
                            <option value="Algeria">
                                Algeria
                            </option>
                            <option value="American Samoa">
                                American Samoa
                            </option>
                            <option value="Andorra">
                                Andorra
                            </option>
                            <option value="Angola">
                                Angola
                            </option>
                            <option value="Anguilla">
                                Anguilla
                            </option>
                            <option value="Antarctica">
                                Antarctica
                            </option>
                            <option value="Antigua and/or Barbuda">
                                Antigua and/or Barbuda
                            </option>
                            <option value="Argentina">
                                Argentina
                            </option>
                            <option value="Armenia">
                                Armenia
                            </option>
                            <option value="Aruba">
                                Aruba
                            </option>
                            <option value="Australia">
                                Australia
                            </option>
                            <option value="Austria">
                                Austria
                            </option>
                            <option value="Azerbaijan">
                                Azerbaijan
                            </option>
                            <option value="Bahamas">
                                Bahamas
                            </option>
                            <option value="Bahrain">
                                Bahrain
                            </option>
                            <option value="Bangladesh">
                                Bangladesh
                            </option>
                            <option value="Barbados">
                                Barbados
                            </option>
                            <option value="Belarus">
                                Belarus
                            </option>
                            <option value="Belgium">
                                Belgium
                            </option>
                            <option value="Belize">
                                Belize
                            </option>
                            <option value="Benin">
                                Benin
                            </option>
                            <option value="Bermuda">
                                Bermuda
                            </option>
                            <option value="Bhutan">
                                Bhutan
                            </option>
                            <option value="Bolivia">
                                Bolivia
                            </option>
                            <option value="Bosnia and Herzegovina">
                                Bosnia and Herzegovina
                            </option>
                            <option value="Botswana">
                                Botswana
                            </option>
                            <option value="Bouvet Island">
                                Bouvet Island
                            </option>
                            <option value="Brazil">
                                Brazil
                            </option>
                            <option value="British Indian Ocean Territory">
                                British Indian Ocean Territory
                            </option>
                            <option value="Brunei Darussalam">
                                Brunei Darussalam
                            </option>
                            <option value="Bulgaria">
                                Bulgaria
                            </option>
                            <option value="Burkina Faso">
                                Burkina Faso
                            </option>
                            <option value="Burundi">
                                Burundi
                            </option>
                            <option value="Cambodia">
                                Cambodia
                            </option>
                            <option value="Cameroon">
                                Cameroon
                            </option>
                            <option value="Canada">
                                Canada
                            </option>
                            <option value="Cape Verde">
                                Cape Verde
                            </option>
                            <option value="Cayman Islands">
                                Cayman Islands
                            </option>
                            <option value="Central African Republic">
                                Central African Republic
                            </option>
                            <option value="Chad">
                                Chad
                            </option>
                            <option value="Chile">
                                Chile
                            </option>
                            <option value="China">
                                China
                            </option>
                            <option value="Christmas Island">
                                Christmas Island
                            </option>
                            <option value="Cocos (Keeling) Islands">
                                Cocos (Keeling) Islands
                            </option>
                            <option value="Colombia">
                                Colombia
                            </option>
                            <option value="Comoros">
                                Comoros
                            </option>
                            <option value="Congo">
                                Congo
                            </option>
                            <option value="Cook Islands">
                                Cook Islands
                            </option>
                            <option value="Costa Rica">
                                Costa Rica
                            </option>
                            <option value="Croatia (Hrvatska)">
                                Croatia (Hrvatska)
                            </option>
                            <option value="Cuba">
                                Cuba
                            </option>
                            <option value="Cyprus">
                                Cyprus
                            </option>
                            <option value="Czech Republic">
                                Czech Republic
                            </option>
                            <option value="Denmark">
                                Denmark
                            </option>
                            <option value="Djibouti">
                                Djibouti
                            </option>
                            <option value="Dominica">
                                Dominica
                            </option>
                            <option value="Dominican Republic">
                                Dominican Republic
                            </option>
                            <option value="East Timor">
                                East Timor
                            </option>
                            <option value="Ecuador">
                                Ecuador
                            </option>
                            <option value="Egypt">
                                Egypt
                            </option>
                            <option value="El Salvador">
                                El Salvador
                            </option>
                            <option value="Equatorial Guinea">
                                Equatorial Guinea
                            </option>
                            <option value="Eritrea">
                                Eritrea
                            </option>
                            <option value="Estonia">
                                Estonia
                            </option>
                            <option value="Ethiopia">
                                Ethiopia
                            </option>
                            <option value="Falkland Islands (Malvinas)">
                                Falkland Islands (Malvinas)
                            </option>
                            <option value="Faroe Islands">
                                Faroe Islands
                            </option>
                            <option value="Fiji">
                                Fiji
                            </option>
                            <option value="Finland">
                                Finland
                            </option>
                            <option value="France">
                                France
                            </option>
                            <option value="France, Metropolitan">
                                France, Metropolitan
                            </option>
                            <option value="French Guiana">
                                French Guiana
                            </option>
                            <option value="French Polynesia">
                                French Polynesia
                            </option>
                            <option value="French Southern Territories">
                                French Southern Territories
                            </option>
                            <option value="Gabon">
                                Gabon
                            </option>
                            <option value="Gambia">
                                Gambia
                            </option>
                            <option value="Georgia">
                                Georgia
                            </option>
                            <option value="Germany">
                                Germany
                            </option>
                            <option value="Ghana">
                                Ghana
                            </option>
                            <option value="Gibraltar">
                                Gibraltar
                            </option>
                            <option value="Greece">
                                Greece
                            </option>
                            <option value="Greenland">
                                Greenland
                            </option>
                            <option value="Grenada">
                                Grenada
                            </option>
                            <option value="Guadeloupe">
                                Guadeloupe
                            </option>
                            <option value="Guam">
                                Guam
                            </option>
                            <option value="Guatemala">
                                Guatemala
                            </option>
                            <option value="Guernsey">
                                Guernsey
                            </option>
                            <option value="Guinea">
                                Guinea
                            </option>
                            <option value="Guinea-Bissau">
                                Guinea-Bissau
                            </option>
                            <option value="Guyana">
                                Guyana
                            </option>
                            <option value="Haiti">
                                Haiti
                            </option>
                            <option value="Heard and Mc Donald Islands">
                                Heard and Mc Donald Islands
                            </option>
                            <option value="Honduras">
                                Honduras
                            </option>
                            <option value="Hong Kong">
                                Hong Kong
                            </option>
                            <option value="Hungary">
                                Hungary
                            </option>
                            <option value="Iceland">
                                Iceland
                            </option>
                            <option value="India">
                                India
                            </option>
                            <option value="Indonesia">
                                Indonesia
                            </option>
                            <option value="Iran (Islamic Republic of)">
                                Iran (Islamic Republic of)
                            </option>
                            <option value="Iraq">
                                Iraq
                            </option>
                            <option value="Ireland">
                                Ireland
                            </option>
                            <option value="Isle of Man">
                                Isle of Man
                            </option>
                            <option value="Israel">
                                Israel
                            </option>
                            <option value="Italy">
                                Italy
                            </option>
                            <option value="Ivory Coast">
                                Ivory Coast
                            </option>
                            <option value="Jamaica">
                                Jamaica
                            </option>
                            <option value="Japan">
                                Japan
                            </option>
                            <option value="Jersey">
                                Jersey
                            </option>
                            <option value="Jordan">
                                Jordan
                            </option>
                            <option value="Kazakhstan">
                                Kazakhstan
                            </option>
                            <option value="Kenya">
                                Kenya
                            </option>
                            <option value="Kiribati">
                                Kiribati
                            </option>
                            <option value="Korea">
                                Korea
                            </option>
                            <option value="Korea, Democratic People's Republic of">
                                Korea, Democratic People's Republic of
                            </option>
                            <option value="Kosovo">
                                Kosovo
                            </option>
                            <option value="Kuwait">
                                Kuwait
                            </option>
                            <option value="Kyrgyzstan">
                                Kyrgyzstan
                            </option>
                            <option value="Lao People's Democratic Republic">
                                Lao People's Democratic Republic
                            </option>
                            <option value="Latvia">
                                Latvia
                            </option>
                            <option value="Lebanon">
                                Lebanon
                            </option>
                            <option value="Lesotho">
                                Lesotho
                            </option>
                            <option value="Liberia">
                                Liberia
                            </option>
                            <option value="Libyan Arab Jamahiriya">
                                Libyan Arab Jamahiriya
                            </option>
                            <option value="Liechtenstein">
                                Liechtenstein
                            </option>
                            <option value="Lithuania">
                                Lithuania
                            </option>
                            <option value="Luxembourg">
                                Luxembourg
                            </option>
                            <option value="Macau">
                                Macau
                            </option>
                            <option value="Macedonia">
                                Macedonia
                            </option>
                            <option value="Madagascar">
                                Madagascar
                            </option>
                            <option value="Malawi">
                                Malawi
                            </option>
                            <option value="Malaysia">
                                Malaysia
                            </option>
                            <option value="Maldives">
                                Maldives
                            </option>
                            <option value="Mali">
                                Mali
                            </option>
                            <option value="Malta">
                                Malta
                            </option>
                            <option value="Marshall Islands">
                                Marshall Islands
                            </option>
                            <option value="Martinique">
                                Martinique
                            </option>
                            <option value="Mauritania">
                                Mauritania
                            </option>
                            <option value="Mauritius">
                                Mauritius
                            </option>
                            <option value="Mayotte">
                                Mayotte
                            </option>
                            <option value="Mexico">
                                Mexico
                            </option>
                            <option value="Micronesia, Federated States of">
                                Micronesia, Federated States of
                            </option>
                            <option value="Moldova, Republic of">
                                Moldova, Republic of
                            </option>
                            <option value="Monaco">
                                Monaco
                            </option>
                            <option value="Mongolia">
                                Mongolia
                            </option>
                            <option value="Montenegro">
                                Montenegro
                            </option>
                            <option value="Montserrat">
                                Montserrat
                            </option>
                            <option value="Morocco">
                                Morocco
                            </option>
                            <option value="Mozambique">
                                Mozambique
                            </option>
                            <option value="Myanmar">
                                Myanmar
                            </option>
                            <option value="Namibia">
                                Namibia
                            </option>
                            <option value="Nauru">
                                Nauru
                            </option>
                            <option value="Nepal">
                                Nepal
                            </option>
                            <option value="Netherlands">
                                Netherlands
                            </option>
                            <option value="Netherlands Antilles">
                                Netherlands Antilles
                            </option>
                            <option value="New Caledonia">
                                New Caledonia
                            </option>
                            <option value="New Zealand">
                                New Zealand
                            </option>
                            <option value="Nicaragua">
                                Nicaragua
                            </option>
                            <option value="Niger">
                                Niger
                            </option>
                            <option value="Nigeria">
                                Nigeria
                            </option>
                            <option value="Niue">
                                Niue
                            </option>
                            <option value="Norfolk Island">
                                Norfolk Island
                            </option>
                            <option value="Northern Mariana Islands">
                                Northern Mariana Islands
                            </option>
                            <option value="Norway">
                                Norway
                            </option>
                            <option value="Oman">
                                Oman
                            </option>
                            <option value="Pakistan">
                                Pakistan
                            </option>
                            <option value="Palau">
                                Palau
                            </option>
                            <option value="Palestine">
                                Palestine
                            </option>
                            <option value="Panama">
                                Panama
                            </option>
                            <option value="Papua New Guinea">
                                Papua New Guinea
                            </option>
                            <option value="Paraguay">
                                Paraguay
                            </option>
                            <option value="Peru">
                                Peru
                            </option>
                            <option value="Philippines">
                                Philippines
                            </option>
                            <option value="Pitcairn">
                                Pitcairn
                            </option>
                            <option value="Poland">
                                Poland
                            </option>
                            <option value="Portugal">
                                Portugal
                            </option>
                            <option value="Puerto Rico">
                                Puerto Rico
                            </option>
                            <option value="Qatar">
                                Qatar
                            </option>
                            <option value="Reunion">
                                Reunion
                            </option>
                            <option value="Romania">
                                Romania
                            </option>
                            <option value="Russian Federation">
                                Russian Federation
                            </option>
                            <option value="Rwanda">
                                Rwanda
                            </option>
                            <option value="Saint Kitts and Nevis">
                                Saint Kitts and Nevis
                            </option>
                            <option value="Saint Lucia">
                                Saint Lucia
                            </option>
                            <option value="Saint Vincent and the Grenadines">
                                Saint Vincent and the Grenadines
                            </option>
                            <option value="Samoa">
                                Samoa
                            </option>
                            <option value="San Marino">
                                San Marino
                            </option>
                            <option value="Sao Tome and Principe">
                                Sao Tome and Principe
                            </option>
                            <option value="Saudi Arabia">
                                Saudi Arabia
                            </option>
                            <option value="Senegal">
                                Senegal
                            </option>
                            <option value="Serbia">
                                Serbia
                            </option>
                            <option value="Seychelles">
                                Seychelles
                            </option>
                            <option value="Sierra Leone">
                                Sierra Leone
                            </option>
                            <option value="Singapore">
                                Singapore
                            </option>
                            <option value="Slovakia">
                                Slovakia
                            </option>
                            <option value="Slovenia">
                                Slovenia
                            </option>
                            <option value="Solomon Islands">
                                Solomon Islands
                            </option>
                            <option value="Somalia">
                                Somalia
                            </option>
                            <option value="South Africa">
                                South Africa
                            </option>
                            <option value="South Georgia South Sandwich Islands">
                                South Georgia South Sandwich Islands
                            </option>
                            <option value="Spain">
                                Spain
                            </option>
                            <option value="Sri Lanka">
                                Sri Lanka
                            </option>
                            <option value="St. Helena">
                                St. Helena
                            </option>
                            <option value="St. Pierre and Miquelon">
                                St. Pierre and Miquelon
                            </option>
                            <option value="Sudan">
                                Sudan
                            </option>
                            <option value="Suriname">
                                Suriname
                            </option>
                            <option value="Svalbard and Jan Mayen Islands">
                                Svalbard and Jan Mayen Islands
                            </option>
                            <option value="Swaziland">
                                Swaziland
                            </option>
                            <option value="Sweden">
                                Sweden
                            </option>
                            <option value="Switzerland">
                                Switzerland
                            </option>
                            <option value="Syrian Arab Republic">
                                Syrian Arab Republic
                            </option>
                            <option value="Taiwan">
                                Taiwan
                            </option>
                            <option value="Tajikistan">
                                Tajikistan
                            </option>
                            <option value="Tanzania, United Republic of">
                                Tanzania, United Republic of
                            </option>
                            <option value="Thailand">
                                Thailand
                            </option>
                            <option value="Togo">
                                Togo
                            </option>
                            <option value="Tokelau">
                                Tokelau
                            </option>
                            <option value="Tonga">
                                Tonga
                            </option>
                            <option value="Trinidad and Tobago">
                                Trinidad and Tobago
                            </option>
                            <option value="Tunisia">
                                Tunisia
                            </option>
                            <option value="Turkey">
                                Turkey
                            </option>
                            <option value="Turkmenistan">
                                Turkmenistan
                            </option>
                            <option value="Turks and Caicos Islands">
                                Turks and Caicos Islands
                            </option>
                            <option value="Tuvalu">
                                Tuvalu
                            </option>
                            <option value="Uganda">
                                Uganda
                            </option>
                            <option value="Ukraine">
                                Ukraine
                            </option>
                            <option value="United Arab Emirates">
                                United Arab Emirates
                            </option>
                            <option value="United Kingdom">
                                United Kingdom
                            </option>
                            <option value="United States">
                                United States
                            </option>
                            <option value="United States minor outlying islands">
                                United States minor outlying islands
                            </option>
                            <option value="Uruguay">
                                Uruguay
                            </option>
                            <option value="Uzbekistan">
                                Uzbekistan
                            </option>
                            <option value="Vanuatu">
                                Vanuatu
                            </option>
                            <option value="Vatican City State">
                                Vatican City State
                            </option>
                            <option value="Venezuela">
                                Venezuela
                            </option>
                            <option value="Vietnam">
                                Vietnam
                            </option>
                            <option value="Virgin Islands (British)">
                                Virgin Islands (British)
                            </option>
                            <option value="Virgin Islands (U.S.)">
                                Virgin Islands (U.S.)
                            </option>
                            <option value="Wallis and Futuna Islands">
                                Wallis and Futuna Islands
                            </option>
                            <option value="Western Sahara">
                                Western Sahara
                            </option>
                            <option value="Yemen">
                                Yemen
                            </option>
                            <option value="Yugoslavia">
                                Yugoslavia
                            </option>
                            <option value="Zaire">
                                Zaire
                            </option>
                            <option value="Zambia">
                                Zambia
                            </option>
                            <option value="Zimbabwe">
                                Zimbabwe
                            </option>
                        </select>
                    </div>
                </div>
                <div class="col-form clearfix">
                    <div class="form-row">
                        <label class="fleld">Email<span class="req">*</span></label>
                        <input title="email" type="email" name="email" value="">
                    </div>
                    <div class="form-row">
                        <label class="fleld">Comments<span class="req">*</span></label>
                        <textarea title="comments" name="comments"></textarea>
                    </div>
                    <div class="form-row clearfix">
                        <button type="submit">SUBMIT</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
        <?php } else {
            echo "<h2 class=\"page-title\">Enquiry Cart Is Empty</h2>";
            echo "<div class=\"clear\"></div>";
        }
        ?>
    </div>
</div>

<!--JavaScript-->
<script src="<?= base_url('public/') ?>js/jquery.min.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>js/menu.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>js/ddsmoothmenu.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>js/drop-down.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>js/jquery.nestedAccordion.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function() {
        if($(window).width() <560 ){
            $(".cart-top").insertAfter(".ddsmoothmenu");
        }

        $('.model-select').on('change', function (event)
        {
            event.preventDefault();
            event.stopPropagation();
            var selected = $(this).find('option:selected');
            var post_data = {
                model : $(this).val()
            };
            var url = '<?=base_url('enquiry-cart/edit/')?>'+selected.data('foo');
            $.post(url, post_data, function (result)
            {
                if (result.result == 1) {
                    window.location.href = '<?=base_url('enquiry-cart')?>';
                }
            }, 'json');
        });

        $('.enquiry-cart-form').on('submit', function (event)
        {
            event.preventDefault();
            event.stopPropagation();

            var form_data = new FormData($(this)[0]);
            $.ajax({
                url: '<?=base_url('cart/submit')?>',
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function(response)
                {
                    if(response.result == 1) {
                        window.location.href = "<?=base_url('thank-you')?>";
                    } else {
                        alert(response.data);
                    }
                }
            });
        });

    });
</script>