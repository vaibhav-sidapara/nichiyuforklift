<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="page-with-banner">
    <div class="container">
        <div class="bred-camb"><a href="<?=base_url('')?>">Home</a><span class="seprster fa fa-angle-right"></span> Dealer Locator</div>
        <h2 class="page-title"> Dealer Locator</h2>
        <div class=" Dealer-Locator clearfix">
            <div class="location">
                <div class="content demo-yx">
                    <div class="loc-inner">
                        <span class="ImageList singapure "><a href="#singapure" title="Singapore"><img src="<?= base_url('public/') ?>images/singapure.png" alt=""></a></span>
                        <span class="ImageList mla "><a href="#mla" title="Malaysia"><img src="<?= base_url('public/') ?>images/Malaysia.png" alt=""></a></span>
                        <span class="ImageList thai "><a href="#thai" title="Thailand"><img src="<?= base_url('public/') ?>images/Thailand.png" alt=""></a></span>
                        <span class="ImageList phi "><a href="#phi" title="Philippines"><img src="<?= base_url('public/') ?>images/Philippines.png" alt=""></a></span>
                        <span class="ImageList viet "><a href="#viet" title="Vietnam"><img src="<?= base_url('public/') ?>images/Vietnam.png" alt=""></a></span>
                        <span class="ImageList indo "><a href="#indo" title="Indonesia"><img src="<?= base_url('public/') ?>images/Indonesia.png" alt=""></a></span>
                        <span class="ImageList Taiwan "><a href="#Taiwan" title="Taiwan"><img src="<?= base_url('public/') ?>images/Taiwan.png" alt=""></a></span>
                        <span class="ImageList sri "><a href="#sri" title="Sri Lanka"><img src="<?= base_url('public/') ?>images/Sri-Lanka.png" alt=""></a></span>
                        <span class="ImageList india "><a href="#india" title="India"><img src="<?= base_url('public/') ?>images/India.png" alt=""></a></span>
                        <span class="ImageList aus "><a href="#aus" title="Australia"><img src="<?= base_url('public/') ?>images/Australia.png" alt=""></a></span>
                        <span class="ImageList pak "><a href="#pak" title="Pakistan"><img src="<?= base_url('public/') ?>images/Pakistan.png" alt=""></a></span>
                        <span class="ImageList kor "><a href="#kor" title="Korea"><img src="<?= base_url('public/') ?>images/Korea.png" alt=""></a></span>
                        <span class="ImageList sa "><a href="#sa" title="South Africa"><img src="<?= base_url('public/') ?>images/South-Africa.png" alt=""></a></span>
                        <img src="<?= base_url('public/') ?>images/location.jpg" alt="" class="locationmap">
                    </div>
                </div>
            </div>
            <div class="location-info one" id="singapure">
                <div class="content mCustomScrollbar ">
                    <div class="">
                        <h2>SINGAPORE </h2>
                        <address>
                            <h3>NICHIYU ASIA PTE. LTD.</h3>
                            Headquarters &amp; Overseas Division:<br>
                            1 Tuas West Street, Singapore 637444<br>
                            Tel: +65 6432-6080<br>
                            Fax: +65 6862-1327<br>
                            Email: <a href="mailto:sales@nichiyu.com.sg">sales@nichiyu.com.sg</a>
                        </address>
                        <address>
                            Sales and after Sales service:<br>
                            <strong>Goldbell Leasing Pte Ltd</strong><br>
                            Material Handling Service and Parts Department<br>
                            14 Benoi Road<br>
                            Singapore 629887<br>
                            Tel No. :  6494 2900<br>
                            Fax No. : 6861 7204<br>
                            Email: <a href="mailto:GBLsales@gbl.com.sg">GBLsales@gbl.com.sg</a>
                        </address>
                    </div>
                </div>
            </div>
            <div class="location-info one" id="mla">
                <div class="content mCustomScrollbar">
                    <div class="">
                        <h2>Malaysia </h2>
                        <address>
                            <h3>NICHIYU ASIA(M)SDN. BHD.</h3>
                            Lot No. 2-36 & 2-38, jalan SU 7<br>
                            Seksyen 26, PersiaranTengkuAmpuan,<br>
                            40400 Shah Alam, Selangor Darul Ehsan<br>
                            Tel: +603-5191 3882 ; 03-5191 3880 ; 03-5191 3887 <br>
                            Fax: +603-5191 5388<br>
                            Email: <a href="mailto:general@nichiyu.com.my">general@nichiyu.com.my</a> URL:<a href="http://www.nichiyu.com.my/home.htm" target="_blank">www.nichiyu.com.my/home.htm</a>
                        </address>
                        <h4> DEALERS :</h4>
                        <strong><em>North Malaysia</em></strong>
                        <address>
                            <h3>TRINEX TECHNOLOGY SDN. BHD.</h3>
                            4, Lorong IKS Juru 8<br>
                            Taman IndustriRinganJuru<br>
                            SimpangEmpat, 14100 <br>
                            Penang, Malaysia<br>
                            Tel : +604-508 5331 <br>
                            Fax : +604-508 5339<br>
                            Email : <a href="mailto:trinex@tm.net.my">trinex@tm.net.my</a><br>
                            contact person : Mr. Michael Lee (Executive Director)
                        </address>
                        <strong><em>South Malaysia</em></strong>
                        <address>
                            <h3> NP EQUIPMENT SDN. BHD.</h3>
                            No. 207, Jalan Ria<br>
                            Taman Bahagia<br>
                            Senai, 81400 <br>
                            Johor, Malaysia<br>
                            Tel : +607-599 4773<br>
                            Fax : +607-599 7695<br>
                            Email : <a href="mailto:kelvin.np@hotmail.com">kelvin.np@hotmail.com</a><br>
                            <strong>contact person :</strong> <br>
                            Mr. Kelvin Choo (Managing Director)<br>
                            Mr. Davis Choo (Manager)
                        </address>
                        <strong><em> East Malaysia</em></strong>
                        <address>
                            <h3>KK FORKLIFT & PARTS SUPPLIES SDN. BHD. </h3>
                            Lot No. 122, No. 17, LorongBuahDuku 3<br>
                            Kolombong Industrial Estate<br>
                            Kota Kinabalu, 88450<br>
                            Sabah, Malaysia<br>
                            Tel : +6088-422 122 <br>
                            Fax : +6088-422 662<br>
                            contact person : Mr. Chua Ken Foo (Managing Director)
                        </address>
                    </div>
                </div>
            </div>
            <div class="location-info one" id="thai">
                <div class="content mCustomScrollbar">
                    <div class="">
                        <h2>Thailand </h2>
                        <address>
                            <h3>NICHIYU ASIA(THAILAND)CO., LTD. </h3>
                            888/9-10 Moo 9,Soi Roongcharoen,<br>
                            LiebKlongSuvarnabhumi Rd.,<br>
                            Bangpla, Bangplee, Samutprakarn 10540<br>
                            Thailand<br>
                            <br>
                            <strong>Sales &amp; Account :</strong> <br>
                            Tel. +66 2 1816717~21,<br>
                            Fax. +66 2 1816722<br>
                            <br>
                            <strong>Service &amp; Parts  :</strong> <br>
                            Tel. +66 2 1816723~27, <br>
                            Fax. +66 2 1816728<br>
                            URL:<a href="http://www.nichiyu.co.th" target="_blank">www.nichiyu.co.th</a>
                        </address>
                    </div>
                </div>
            </div>
            <div class="location-info one" id="phi">
                <div class="content mCustomScrollbar">
                    <div class="">
                        <h2>Philippines </h2>
                        <address>
                            <h3>NichiyuAsialift Philippines, Inc.</h3>
                            No.9 Macario Flores Street,<br>
                            Brgy.Sto, Rosario,
                            SilangangPateros Metro, <br>
                            Manila Philippines<br>
                            Tel : +63 2 640 1088 <br>
                            Fax :  +63 2 640 4488<br>
                            Email : <a href="mailto:asialift!nichiyuph.com">asialift!nichiyuph.com</a>
                        </address>
                    </div>
                </div>
            </div>
            <div class="location-info one" id="viet">
                <div class="content mCustomScrollbar">
                    <div class="">
                        <h2>Vietnam</h2>
                        <address>
                            <h3>NVN COMPANY LTD.</h3>
                            64/1 Xuan Dieu St. <br>
                            Tan Binh Dist. <br>
                            Ho Chi Minh, Vietnam <br>
                            Tel: +84 8 8116078 <br>
                            Fax: +84 8 8116077 <br>
                            Email: <a href="mailto:nichiyuvn@hcm.vnn.vn">nichiyuvn@hcm.vnn.vn</a>
                        </address>
                    </div>
                </div>
            </div>
            <div class="location-info one" id="indo">
                <div class="content mCustomScrollbar">
                    <div class="">
                        <h2>Indonesia </h2>
                        <address>
                            <h3>PT. BERCA MANDIRI PERKASA</h3>
                            Jl. PangeranJayakarta 149A <br>
                            Jakarta 10730, Indonesia<br>
                            Tel: +62 21 6006125, 6280900, 6399190<br>
                            Fax: +62 21 6399191<br>
                            URL:<a href="http://www.berca-mp.co.id" target="_blank">www.berca-mp.co.id</a>
                        </address>
                    </div>
                </div>
            </div>
            <div class="location-info one" id="Taiwan">
                <div class="content mCustomScrollbar">
                    <div class="">
                        <h2>Taiwan</h2>
                        <address>
                            <h3>TAIPEI TRADING CO., LTD.</h3>
                            8/F, 363 Songjiang Rd., Taipei, <br>
                            Taiwan 10482<br>
                            Tel: +886 2 2503 5688 <br>
                            Fax: +886 2 2503 6005<br>
                            Email: <a href="mailto:ttcfl@tpts5.seed.net.tw">ttcfl@tpts5.seed.net.tw</a>
                        </address>
                    </div>
                </div>
            </div>
            <div class="location-info one" id="sri">
                <div class="content mCustomScrollbar">
                    <div class="">
                        <h2>Sri Lanka </h2>
                        <address>
                            <h3>DAX INTEGRATED SYSTEMS PVT. LTD.</h3>
                            No: 282/3, Pipe Road,<br>
                            Koswatta, Battaramulla,<br>
                            Sri Lanka<br>
                            Tel: +94 11 5569101/102 <br>
                            Fax: +94 11 5569100<br>
                            Email:<a href="mailto: dhammika_dax@stmail.lk"> dhammika_dax@stmail.lk</a>
                        </address>
                    </div>
                </div>
            </div>
            <div class="location-info one" id="india">
                <div class="content mCustomScrollbar ">
                    <div class="">
                        <h2>India </h2>
                        <address>
                            <h3>NICHIYU FORKLIFTS INDIA Pvt. Ltd.</h3>
                            L.A.S Tower, No. 61, Velachery Road,<br>
                            Little Mount, Saidapet,<br>
                            Chennai - 600015
                            INDIA<br>
                            Tel : +91-44-22354214 to 603 <br>
                            Fax : +91-44-22354215<br>
                            Website : <a href="www.nichiyu.in" target="_blank">www.nichiyu.in</a><br>
                            Email : <a href="mailto:service@nichiyu.in ">service@nichiyu.in </a>
                        </address>
                        <h4>DEALERS : </h4>
                        <strong><em>Northern Sector</em></strong>
                        <address>
                            <h3>WATRANA TRACTION Pvt. Ltd.</h3>
                            B-101, Gujranwala Town Part-1 <br>
                            Delhi - 110009
                            INDIA <br>
                            Tel : +91-11-27456600 / 91-11-27445100 / 91-11-27247200 <br>
                            Fax : +91-11-27465600 <br>
                            Email : <a href="mailto:sales@watranaevs.com">sales@watranaevs.com</a> <br>
                            contact person : Mr. Rajeev Watrana
                        </address>
                        <strong><em>Western Sector</em></strong>
                        <address>
                            <h3>SKAN MARINE SERVICES PVT. LTD.</h3>
                            310/311, RAHEJA ARCADE SECTOR-11 CBD <br>
                            BELAPUR<br>
                            NAVI MUMBAI - 400614<br>
                            Tel : +91 02240809301/02<br>
                            Fax : 4080-9304<br>
                            Email: <a href="mailto:info@skansales.in">info@skansales.in</a>
                        </address>
                        <strong><em>Central Sector</em></strong>
                        <address>
                            <h3>PRIME FORKLIFTERS PVT.LTD.</h3>
                            26, Bangalore Co-operative Industrial Estate<br>
                            Old Madras Road, Dooravaninagar, <br>
                            Bangalore - 560016<br>
                            Tel : +91 80-28530000 <br>
                            Email: <a href="mailto:sales@primeforklifters.in">sales@primeforklifters.in</a>
                        </address>
                    </div>
                </div>
            </div>
            <div class="location-info one" id="aus">
                <div class="content mCustomScrollbar">
                    <div class="">
                        <h2>Australia </h2>
                        <address>
                            <h3>NFM FORKLIFTS PTY. LTD.</h3>
                            10 Walker Place<br>
                            Wetherill Park NSW 2164<br>
                            Tel: +61 2-9765 2040 <br>
                            Fax: +61 2 9725 1974<br>
                            URL: <a href="http://www.nfmforklifts.com.au/" target="_blank">www.nfmforklifts.com.au/</a>
                        </address>
                    </div>
                </div>
            </div>
            <div class="location-info one" id="pak">
                <div class="content mCustomScrollbar">
                    <div class="">
                        <h2>Pakistan </h2>
                        <address>
                            <h3>TRANSLINK INTERNATIONAL</h3>
                            57 C/1 Gulberg 3, MM Alam Road<br>
                            Lahore, Pakistan 54660<br>
                            Tel: +92 42 5877513/514 <br>
                            Fax: +92 42 5877515<br>
                            Email: <a href="mailto:transl@brain.net.pk">transl@brain.net.pk</a>/<a href="mailto:gps@brain.net.pk">gps@brain.net.pk</a>
                        </address>
                    </div>
                </div>
            </div>
            <div class="location-info one" id="kor">
                <div class="content mCustomScrollbar">
                    <div class="">
                        <h2>Korea</h2>
                        <strong> <em> South Korea</em></strong>
                        <address>
                            <h3>DOOSUNG LOGISTICS MACHINERY CO., LTD.</h3>
                            59-9, Samgye-ri, Cheongbuk-myun, <br>
                            Pyeongtaek-si, <br>
                            Gyeonggido 451-832<br>
                            Tel: +82 31 684 2915 <br>
                            Fax: +82 31 684 2910<br>
                            Email: <a href="mailto:ybmun@doosungfl.com">ybmun@doosungfl.com</a><br>
                            URL: <a href="http://www.doosungfl.com" target="_blank">www.doosungfl.com</a>
                        </address>
                        <address>
                            <h3>HYUKSHIN(RENOVARE) CO., LTD.</h3>
                            158 Eoryoung2-gil, Seonghwan-eup,<br>
                            ' Seobuk-gu, Cheonan-shi,<br>
                            Chungnam 331-805<br>
                            <br>
                            tel: +82 41 588 4333 <br>
                            fax: +82 41 588 4336<br>
                            mail:<a href="mailto: bassbass50@renovare.kr"> bassbass50@renovare.kr</a><br>
                            URL: <a href="http://www.renovare.kr" target="_blank">www.renovare.kr</a>
                        </address>
                    </div>
                </div>
            </div>
            <div class="location-info one" id="sa">
                <div class="content mCustomScrollbar">
                    <div class="">
                        <h2>South Africa</h2>
                        <address>
                            <h3>A SQUARE FORKLIFT (PTY) LTD.</h3>
                            8 Delfos Avenue Alrode<br>
                            South Alberton <br>
                            Tel: +27 11 900 1777 <br>
                            Fax: +27 11 900 3993<br>
                            Email: <a href="mailto:craig@asquare.co.za">craig@asquare.co.za</a><br>
                            URL: <a href="http://www.forklift.co.za" target="_blank">www.forklift.co.za</a>
                        </address>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!--JavaScript-->
<script src="<?= base_url('public/') ?>js/jquery.min.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>js/menu.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>js/ddsmoothmenu.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>js/drop-down.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>js/jquery.nestedAccordion.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>js/jquery.magnific-popup.min.js"></script>
<script src="<?= base_url('public/') ?>js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<?= base_url('public/') ?>js/location.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function() {
        if($(window).width() <560 ){
            $(".cart-top").insertAfter(".ddsmoothmenu");
        }
    });
</script>
<script>
    (function($){
        $(window).load(function(){

            $("#content-1").mCustomScrollbar({
                theme:"minimal"
            });
            $(".demo-yx").mCustomScrollbar({
                axis:"yx"
            });

            $(".scrollTo a").click(function(e){
                e.preventDefault();
                var $this=$(this),
                    rel=$this.attr("rel"),
                    el=rel==="content-y" ? ".demo-y" : rel==="content-x" ? ".demo-x" : ".demo-yx",
                    data=$this.data("scroll-to"),
                    href=$this.attr("href").split(/#(.+)/)[1],
                    to=data ? $(el).find(".mCSB_container").find(data) : el===".demo-yx" ? eval("("+href+")") : href,
                    output=$("#info > p code"),
                    outputTXTdata=el===".demo-yx" ? data : "'"+data+"'",
                    outputTXThref=el===".demo-yx" ? href : "'"+href+"'",
                    outputTXT=data ? "$('"+el+"').find('.mCSB_container').find("+outputTXTdata+")" : outputTXThref;
                $(el).mCustomScrollbar("scrollTo",to);
                output.text("$('"+el+"').mCustomScrollbar('scrollTo',"+outputTXT+");");
            });

        });
    })(jQuery);
</script>