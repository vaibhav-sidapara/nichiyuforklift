<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="page-with-banner">
    <div class="container">
        <div class="left-menu">
            <h2>Categories<i class="click"></i><i class="normal-icon"></i></h2>
            <div class="menu">
                <ul id="accordion">
                    <?php $this->view('products_list_view', ['active_slug' => $active_slug]); ?>
                </ul>
            </div>
        </div>
        <div class="right-col">
            <div class="bred-camb"><a href="index.html">Home</a><span class="seprster fa fa-angle-right"></span>Our Products</div>
            <h2 class="page-title">Products</h2>
            <div class="pagin-top clearfix">
                <div class="pagen-wrap pagein">
                    <ul>
                        <li class="pre"><a href="#"></a></li>
                        <li><a href="#" class="active">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li class="nex"><a href="#"></a></li>
                    </ul>

                </div>
            </div>

            <div class="listing-wrap clearfix">
                <?php foreach ($this->session->userdata('categories_structure') as $category) { ?>
                <div class="bx-product">
                    <figure>
                        <img src="<?=base_url('public/'.$category->image)?>" alt="">
                        <figcaption>
                            <a href="<?=base_url('products/category/'.$category->slug)?>" class="view">VIEW</a>
                        </figcaption><h2><?=$category->name?></h2>
                    </figure>
                    <article>
                        <h2><?=$category->name?></h2>
                    </article>
                </div>
                <?php } ?>
            </div>
            <div class="pagin-bottom clearfix">
                <div class="pagen-wrap pagein">
                    <ul>
                        <li class="pre"><a href="#"></a></li>
                        <li><a href="#" class="active">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li class="nex"><a href="#"></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>


<!--JavaScript-->
<script src="<?= base_url('public/') ?>js/jquery.min.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>js/menu.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>js/ddsmoothmenu.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>js/drop-down.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>js/jquery.nestedAccordion.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>js/bookmarkscroll.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>js/left-menu.js" type="text/javascript"></script>
<script src="<?= base_url('public/') ?>js/jquery.jcarousel.js" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function() {
        if($(window).width() <560 ){
            $(".cart-top").insertAfter(".ddsmoothmenu");
        }
    });
</script>

<script type="text/javascript">
    $("#accordion > li > span").click(function(){

        if(false == $(this).next().is(':visible')) {
            $('#accordion ul').slideUp(300);
        }
        $(this).next().slideToggle(300);
    });

    $(".click").click(function(){
        $(".menu").slideToggle(300);
    });


    //$('#accordion ul:eq(0)').show();

</script>
<script type="text/javascript">
    $(document).ready(function(){
        $(".Sub span").click(function(e) {
            e.preventDefault();
            $("span").removeClass("active-Sub");
            $(this).addClass("active-Sub");
        })
    });
</script>