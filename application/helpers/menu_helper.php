<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if(!function_exists('active_link')) {
    function activate_controller($controller) {
        $CI = get_instance();
        $class = $CI->router->fetch_class();
        return ($class == $controller) ? 'Select' : '';
    }

    function activate_method($controller) {
        $CI = get_instance();
        $method = $CI->router->fetch_method();
        return ($method == $controller) ? 'Select' : '';
    }
}